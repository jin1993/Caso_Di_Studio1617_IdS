package errorhandling;

import java.awt.Component;

import javax.swing.JOptionPane;


public class MessageDisplayer {

	
	public static void showErrorMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	public static void showErrorMessage(Component comp, String message) {
		JOptionPane.showMessageDialog(comp, message);
	}

	
	public static String showInputMessage(Component parentComponent, Object message, String title, int messageType) {
		return JOptionPane.showInputDialog(parentComponent, message, title, messageType);
	}
}
