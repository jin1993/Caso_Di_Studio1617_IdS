package presentation;

import java.util.List;

import javax.mail.MessagingException;

import business.OperazioneNonCompletata;
import business.casiduso.AppServiceFactory;
import business.casiduso.AppServiceInt;
import errorhandling.MessageDisplayer;

public class ApplicationController implements ApplicationControllerInt {

	public ApplicationController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object handleRequest(String serviceName, List<Object> parameters) throws Exception {
		
		AppServiceFactory appServiceFactory = AppServiceFactory.getInstance();
		Object returnObj = null;
		
		try {
			AppServiceInt appService = appServiceFactory.getService(serviceName);
			appService.init(parameters);
			returnObj = appService.execute();
			
		} catch (OperazioneNonCompletata e) {
			MessageDisplayer.showErrorMessage(e.toString());
		}
		
		return returnObj;
	}

}
