package presentation;

import business.casiduso.UserRole;

public class Logger {

		private static Logger logger;
		private static UserRole userRole;
		private static String username;
		
		private Logger() {
		}
		
		public static Logger getInstance()
		{
			if(logger == null)
				logger = new Logger();
			
			return logger;
		}

		public UserRole getUserRole() {
			return userRole;
		}

		public void setUserRole(UserRole userRole) {
			Logger.userRole = userRole;
		}
		
		public void setUsername(String uName)
		{
			Logger.username = uName;
		}
		
		public String getUsername()
		{
			return username;
		}

	}

