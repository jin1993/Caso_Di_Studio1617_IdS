package presentation;

import business.casiduso.UserRole;
import presentation.view.LoginView;

public class Main {
	private static Logger logStatus;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			logStatus = Logger.getInstance();
			logStatus.setUserRole(UserRole.ANONIMO);
			//logStatus.setUsername("dip0001");
			String viewResource = ViewMapping.homeView.get(logStatus.getUserRole());

			LoginView l = new LoginView();
			l.main(null);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
