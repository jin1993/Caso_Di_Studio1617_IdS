package presentation;

import java.util.List;

public interface ApplicationControllerInt {	
	
	public Object handleRequest(String serviceName, List<Object> parameters) throws Exception;
	
}
