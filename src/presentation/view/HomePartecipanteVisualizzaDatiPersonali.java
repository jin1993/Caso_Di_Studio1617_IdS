package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import business.bo.Partecipante;
import business.bo.UserSingleton;
import integration.factories.DaoFactory;
import integration.interfaces.DaoPartecipante;

import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class HomePartecipanteVisualizzaDatiPersonali implements ActionListener{

	private Partecipante parameter = null;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePartecipanteVisualizzaDatiPersonali window = new HomePartecipanteVisualizzaDatiPersonali();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePartecipanteVisualizzaDatiPersonali() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 612, 451);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button_3 = new Button("Indietro");
		frame.getContentPane().add(button_3, BorderLayout.SOUTH);
		button_3.addActionListener(this);
		TextArea textArea = new TextArea();
		//frame.getContentPane().add(a, BorderLayout.CENTER);
		
		
		DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoPartecipante p = df.getDaoPartecipante();
		this.parameter = new Partecipante(p.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()));
		
		JTextArea a = new JTextArea("Nome:" + parameter.getNome() + "\n" + "Cognome:" + parameter.getCognome() + "\n"+
		"Email:" + parameter.getEmail() + "\n"+"Codice Fiscale:" + parameter.getCodf() + "\n"+
				"Sesso:" + parameter.getSesso().toString() + "\n"+"Username:" + parameter.getUsername() + "\n"+
		"Data di nascita:" + parameter.getDatanascita().toString() + "\n"+"Citt�:" + parameter.getCitta() + "\n"+
				"via:" + parameter.getVia() + "\n"+"N. Civico:" + parameter.getNcivico() + "\n"+"Data certificato:" + parameter.getDatacert().toString() + "\n");
		a.setEditable(false);
		frame.getContentPane().add(a, BorderLayout.CENTER);
		/*a.insert("Nome:" + parameter.getNome() + "\n" + "Cognome:" + parameter.getCognome() + "\n"+
		"Email:" + parameter.getEmail() + "\n"+"Codice Fiscale:" + parameter.getCodf() + "\n"+
				"Sesso:" + parameter.getSesso().toString() + "\n"+"Username:" + parameter.getUsername() + "\n"+
		"Data di nascita:" + parameter.getDatanascita().toString() + "\n"+"Citt�:" + parameter.getCitta() + "\n"+
				"via:" + parameter.getVia() + "\n"+"N. Civico:" + parameter.getNcivico() + "\n"+"Data certificato:" + parameter.getDatacert().toString() + "\n", 0);
	*/
	}
	
	public void init(Partecipante par) {
		DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoPartecipante p = df.getDaoPartecipante();
		this.parameter = new Partecipante(p.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		frame.setVisible(false);	
		HomePartecipante hp = new HomePartecipante();
		hp.main(null);
	}

}
