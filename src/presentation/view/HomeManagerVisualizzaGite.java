package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;

import business.bo.Gita;
import business.bo.UserSingleton;
import business.to.GitaTO;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoManager;
import presentation.FrontController;

public class HomeManagerVisualizzaGite implements ActionListener{

	private List<GitaTO> gite;
	private int pos = -1;
	private JFrame frame;
	JComboBox<String> c;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeManagerVisualizzaGite window = new HomeManagerVisualizzaGite();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomeManagerVisualizzaGite() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Modifica Gita");
		verticalBox.add(button);
		button.addActionListener(this);
		
		Button button_1 = new Button("Annulla Gita");
		verticalBox.add(button_1);
		button_1.addActionListener(this);
		
		
		Button button_3 = new Button("Indietro");
		verticalBox.add(button_3);
		button_3.addActionListener(this);
		
		//java.awt.List list = new java.awt.List();
		c = new JComboBox<String>();
		
		DaoFactory f = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita dg = f.getDaoGita();
		DaoManager dm = f.getDaoManager();
		gite=dg.getGita(dm.getManager(UserSingleton.getInstance().getManager().getUsername()));
		
		for(int i = 0; gite.size()>i;i++) {
			//while(!g.get(i).getStato().equals(Stato.TERMINATA)) {
				c.addItem("Gita: " + gite.get(i).getId() +
						", Tipo : " + gite.get(i).getTipo().toString() +
						", Data : " + gite.get(i).getData().toString() +
						", Costo: " + gite.get(i).getCosto() +
						", Stato: " + gite.get(i).getStato().toString());
			//}
		}
		
		
		frame.getContentPane().add(c, BorderLayout.CENTER);
		c.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pos = c.getSelectedIndex();
				UserSingleton.getInstance().setGcorrente( new Gita(gite.get(pos)));
			}
		});
	}

	public void init(List<GitaTO> p) {
		this.gite=p;
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(pos > -1 && pos<gite.size()) {
		switch (e.getActionCommand()) {
		case "Modifica Gita":
			try {
				frame.setVisible(false);
				modificagita();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
			
		case "Annulla Gita":
			try {
				frame.setVisible(false);
				annullagita();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		case "Indietro":
			try {
				frame.setVisible(false);
				HomeManager hm = new HomeManager();
				hm.main(null);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		}
	}
	}

	private void modificagita() {
		HomeManagerModificaGita hmmg = new HomeManagerModificaGita();
		hmmg.init(gite.get(pos));
		hmmg.main(null);
	}

	private void annullagita() {
		List<Object> l=new LinkedList<>();
		l.add(gite.get(pos).getId());
		l.add("ANNULLATA");
		FrontController fc = new FrontController();
		try {
			fc.processRequest("CambiaStatoGita", l);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MessageDisplayer.showErrorMessage("Gita annullata");
		HomeManager hm = new HomeManager();
		hm.main(null);
	}

}
