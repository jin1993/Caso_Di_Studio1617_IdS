package presentation.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.Box;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.awt.Label;
import java.awt.Panel;
import javax.swing.SwingConstants;

import business.casiduso.UserRole;
import business.to.Sesso;
import errorhandling.MessageDisplayer;
//import javafx.scene.control.DatePicker;
import presentation.FrontController;
import presentation.Logger;

import java.awt.Component;
import java.awt.Button;
import javax.swing.JRadioButton;

public class RegistrazioneView implements ActionListener {

	private JFrame frmRegistrazione;
	private TextField textField;
	private TextField textField_1;
	private TextField textField_2;
	private TextField textField_3;
	private TextField textField_4;
	private TextField textField_5;
	private TextField textField_6;
	private TextField textField_7;
	private TextField textField_8;
	private TextField textField_9;
	private TextField textField_10;
	private TextField textField_11;
	private TextField textField_12;
	private TextField textField_13;
	private TextField textField_14;
	private Sesso s;
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private String codf;
	private Sesso sesso;
	private String via;
	private String ncivico;
	private String citta;
	private LocalDate datanascita;
	private int numtessanit;
	private String Certificato;
	private LocalDate datacert;
	private String pws;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistrazioneView window = new RegistrazioneView();
					window.frmRegistrazione.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegistrazioneView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegistrazione = new JFrame();
		frmRegistrazione.setTitle("Registrazione");
		frmRegistrazione.setBounds(100, 100, 675, 443);
		frmRegistrazione.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Box horizontalBox_12 = Box.createHorizontalBox();
		frmRegistrazione.getContentPane().add(horizontalBox_12, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		horizontalBox_12.add(panel);

		Box verticalBox = Box.createVerticalBox();
		panel.add(verticalBox);

		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);

		Label label_11 = new Label("Nome");
		horizontalBox.add(label_11);

		Box horizontalBox_1 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_1);

		Label label = new Label("Cognome");
		horizontalBox_1.add(label);

		Box horizontalBox_14 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_14);

		Label label_12 = new Label("Sesso");
		horizontalBox_14.add(label_12);

		Box horizontalBox_2 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_2);

		Label label_1 = new Label("via");
		horizontalBox_2.add(label_1);

		Box horizontalBox_3 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_3);

		Label label_2 = new Label("numero civico");
		horizontalBox_3.add(label_2);

		Box horizontalBox_4 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_4);

		Label label_3 = new Label("citt\u00E0");
		horizontalBox_4.add(label_3);

		Box horizontalBox_9 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_9);

		Label label_8 = new Label("data di nascita");
		horizontalBox_9.add(label_8);

		Box horizontalBox_5 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_5);

		Label label_4 = new Label("numero tessera sanitaria");
		horizontalBox_5.add(label_4);

		Box horizontalBox_6 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_6);

		Label label_5 = new Label("Certificato");
		horizontalBox_6.add(label_5);

		Box horizontalBox_7 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_7);

		Label label_6 = new Label("data certificato");
		horizontalBox_7.add(label_6);

		Box horizontalBox_8 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_8);

		Label label_7 = new Label("email");
		horizontalBox_8.add(label_7);

		Box horizontalBox_10 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_10);

		Label label_9 = new Label("username");
		horizontalBox_10.add(label_9);

		Box horizontalBox_11 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_11);

		Label label_10 = new Label("password");
		horizontalBox_11.add(label_10);

		Box horizontalBox_16 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_16);

		Label label_13 = new Label("CodiceFiscale");
		horizontalBox_16.add(label_13);

		Panel panel_1 = new Panel();
		horizontalBox_12.add(panel_1);

		Box verticalBox_1 = Box.createVerticalBox();
		panel_1.add(verticalBox_1);

		textField = new TextField();
		textField.setColumns(30);
		verticalBox_1.add(textField);

		textField_1 = new TextField();
		verticalBox_1.add(textField_1);

		Box horizontalBox_15 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_15);

		JRadioButton rdbtnNewRadioButton = new JRadioButton("Maschio");
		horizontalBox_15.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.addActionListener(new radioAction());

		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Femmina");
		horizontalBox_15.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.addActionListener(new radioAction2());

		textField_2 = new TextField();
		verticalBox_1.add(textField_2);

		textField_3 = new TextField();
		verticalBox_1.add(textField_3);

		textField_4 = new TextField();
		verticalBox_1.add(textField_4);

		textField_5 = new TextField();
		verticalBox_1.add(textField_5);

		textField_6 = new TextField();
		verticalBox_1.add(textField_6);

		textField_7 = new TextField();
		verticalBox_1.add(textField_7);

		textField_8 = new TextField();
		verticalBox_1.add(textField_8);

		textField_9 = new TextField();
		verticalBox_1.add(textField_9);

		textField_10 = new TextField();
		verticalBox_1.add(textField_10);

		textField_11 = new TextField();
		verticalBox_1.add(textField_11);

		textField_14 = new TextField();
		verticalBox_1.add(textField_14);

		Box horizontalBox_13 = Box.createHorizontalBox();
		frmRegistrazione.getContentPane().add(horizontalBox_13, BorderLayout.SOUTH);

		Button button_1 = new Button("Annulla");
		button_1.addActionListener(this);
		horizontalBox_13.add(button_1);

		Button button = new Button("Registrati");
		button.addActionListener(this);
		horizontalBox_13.add(button);
	}

	public class radioAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			s = Sesso.M;
		}

	}

	public class radioAction2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			s = Sesso.F;
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Registrati":
			try {
				username = textField_10.getText();
				email = textField_9.getText();
				nome = textField.getText();
				cognome = textField_1.getText();
				codf = textField_14.getText();
				citta = textField_4.getText();
				datacert = LocalDate.parse(textField_8.getText());
				datanascita = LocalDate.parse(textField_5.getText());
				ncivico = textField_3.getText();
				numtessanit = Integer.parseInt(textField_6.getText());
				sesso = s;
				via = textField_2.getText();
				pws = textField_11.getText();
				registra();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		case "Annulla":
			try {
				frmRegistrazione.setVisible(false);
				LoginView l = new LoginView();
				l.main(null);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		}

	}

	private void registra() throws Exception {
		if (textField.getText().equalsIgnoreCase("") || textField_1.getText().equalsIgnoreCase("")
				|| textField_2.getText().equalsIgnoreCase("") || textField_3.getText().equalsIgnoreCase("")
				|| textField_4.getText().equalsIgnoreCase("") || textField_5.getText().equalsIgnoreCase("")
				|| textField_6.getText().equalsIgnoreCase("") || textField_8.getText().equalsIgnoreCase("")
				|| textField_9.getText().equalsIgnoreCase("") || textField_10.getText().equalsIgnoreCase("")
				|| textField_11.getText().equalsIgnoreCase("") || textField_14.getText().equalsIgnoreCase(""))
			MessageDisplayer.showErrorMessage("Compilare tutti i campi");
		else {
			List<Object> parameter = new ArrayList<Object>();

			Collections.addAll(parameter, username, email, nome, cognome, codf, citta, datacert, datanascita, ncivico,
					numtessanit, sesso, via, pws);

			FrontController frontController = new FrontController();
			frontController.processRequest("Registrazione", parameter);
			MessageDisplayer.showErrorMessage("Registrazione effettuata");

		}
	}

}
