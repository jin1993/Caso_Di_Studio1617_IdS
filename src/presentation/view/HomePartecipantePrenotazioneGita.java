package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import business.bo.Optional;
import business.bo.UserSingleton;
import business.casiduso.CalcolaCostoFinale;
import business.to.GitaTO;
import business.to.OptionalTO;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoOptional;
//import javafx.util.converter.LocalDateStringConverter;
import presentation.FrontController;

import java.awt.Panel;
import java.awt.Label;
import java.awt.FlowLayout;
import java.awt.Checkbox;
import javax.swing.JCheckBoxMenuItem;
import java.awt.TextArea;

public class HomePartecipantePrenotazioneGita implements ActionListener{

	private GitaTO g;
	private JFrame frame;
	private JCheckBox c1;
	private JCheckBox c2;
	private JCheckBox c3;
	private JTextArea textArea;
	private JTextArea a;
	int op1;
	int op2;
	int op3;
	private List<Integer> opt = new LinkedList<Integer>();
	FrontController fcController = new FrontController();
	boolean c1b = false;
	boolean c2b = false;
	boolean c3b = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePartecipantePrenotazioneGita window = new HomePartecipantePrenotazioneGita();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePartecipantePrenotazioneGita() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 496, 276);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Prenota Gita");
		verticalBox.add(button);
		button.addActionListener(this);
		
		Button button_3 = new Button("Indietro");
		button_3.addActionListener(this);
		verticalBox.add(button_3);
		
		Panel panel_2 = new Panel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		frame.getContentPane().add(panel_2, BorderLayout.CENTER);
		
		Box verticalBox_1 = Box.createVerticalBox();
		panel_2.add(verticalBox_1);
		
		Panel panel = new Panel();
		verticalBox_1.add(panel);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		Label label = new Label("Scegli Optional");
		panel.add(label);
		
		Panel panel_1 = new Panel();
		verticalBox_1.add(panel_1);
		
		c1 = new JCheckBox("Merenda");
		panel_1.add(c1);
		c1.setSelected(false);
		c1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				c1b = true;
				
			}
		});
		
		
		c2 = new JCheckBox("Pranzo");
		panel_1.add(c2);
		c2.setSelected(false);
		c2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				c2b = true;
				
			}
		});
		
		
		c3 = new JCheckBox("Visita a sito");
		panel_1.add(c3);
		c3.setSelected(false);
		c3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				c3b = true;
				
			}
		});
		
		
		
		Panel panel_3 = new Panel();
		verticalBox_1.add(panel_3);
		
		textArea = new JTextArea();
		panel_3.add(textArea);
		
		a =new JTextArea();
		panel_3.add(a);
		
		DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional dop = df.getDaoOptional();
		g = UserSingleton.getInstance().getGcorrente();
		
		
		
		}
		
	

	public void init(GitaTO gita) {
		// TODO Auto-generated method stub
		this.g=gita;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
			
			switch (e.getActionCommand()) {
			case "Prenota Gita":
				try {
					
					DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
					DaoOptional dop = df.getDaoOptional();
					g = UserSingleton.getInstance().getGcorrente();
					
					
					
					System.out.println("c1 "+c1.isSelected());
					System.out.println("c2 "+c2.isSelected());
					System.out.println("c3 "+c3.isSelected());
						textArea.setText("Gita: " + g.getTipo().toString()+"\nData Gita: " + g.getData().toString()+"\nCosto Gita: " + g.getCosto());
					
						if(c1.isSelected()==true || c2.isSelected()==true || c3.isSelected()==true) {
							
						textArea.setText("\nOptional Scelti: \n" );
						if(c1b==true){
							op1 = 2;
							opt.add(op1);
						}
						if(c2b==true){
							op2 = 1;
							opt.add(op2);
						}
							
						if(c3b==true){
							op3=3;
							opt.add(op3);
						}
							
						if(opt!=null){
							for(int i = 0; opt.size()>i ; i++)
							{
								
								a.setText("\nNome Optional: " + dop.getOptional(opt.get(i)).getNome());
								a.setText("\nCosto Optional: " +dop.getOptional(opt.get(i)).getCosto());
							}
						}
						
						/*try {
							List<Object> l = null;
							l.add(UserSingleton.getInstance().getPartecipante().getUsername());
							l.add(g.getId());
							fcController.processRequest("CalcolaCostoFinale",(List<Object>) l);
							
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
						try {
							List<Object> l = new LinkedList<Object>();
							l.add(g.getId());
							l.add(UserSingleton.getInstance().getPartecipante().getUsername());
							
							l.add(opt);
							a.setText("COSTO TOTALE: " + 
									fcController.processRequest("CalcolaCostoFinale",(List<Object>) l));
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					
					List<Object> l =new LinkedList<Object>();
					System.out.println("GITA "+UserSingleton.getInstance().getGcorrente().getId());
					l.add(0, UserSingleton.getInstance().getGcorrente().getId());
					l.add(1,UserSingleton.getInstance().getPartecipante().getUsername());
					Date input = new Date();
					LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					l.add(2, date);
					List<Integer> optdm = new LinkedList<Integer>() ;
					
					System.out.println("opt.size "+ opt.size());
					
					for(int i = 0; opt.size()>i ; i++)
					{
						optdm.add(opt.get(i));
					}
					l.add(optdm);
					fcController.processRequest("AggiungiPrenotazione", l);
					
					MessageDisplayer.showErrorMessage("Prenotazione Effettuata!");
					frame.setVisible(false);
					
					HomePartecipanteVisualizzaGita hpvg = new HomePartecipanteVisualizzaGita();
						}} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
				
			case "Indietro":
				try {
					frame.setVisible(false);
					HomePartecipante p= new HomePartecipante();
					p.main(null);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				break;
		}}
		
	

}
