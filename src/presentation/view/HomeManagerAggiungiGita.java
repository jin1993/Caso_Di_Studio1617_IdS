package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JRadioButton;

import business.bo.UserSingleton;
import business.to.GitaTO;
import business.to.Stato;
import business.to.Tipo;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoManager;
import presentation.FrontController;
import presentation.view.HomeManagerModificaGita.radioAction;
import presentation.view.HomeManagerModificaGita.radioAction2;
import presentation.view.HomeManagerModificaGita.radioAction3;

import java.awt.Checkbox;

public class HomeManagerAggiungiGita implements ActionListener {
	private int id;
	private LocalDate data;
	private String tipe;
	private Tipo tipo;
	private int minpart;
	private int maxpart;
	private float costo;
	private String state;
	private Stato stato;
	private int manager;
	
	protected List<String> optional;
	//private GitaTO g;

	TextField textField;
	TextField textField_1;
	TextField textField_2;
	TextField textField_3;
	TextField textField_4;
	JCheckBox checkbox;
	JCheckBox checkbox_1;
	JCheckBox checkbox_2;

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeManagerAggiungiGita window = new HomeManagerAggiungiGita();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomeManagerAggiungiGita() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 606, 407);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Aggiungi Gita");
		verticalBox.add(button);
		button.addActionListener(this);
		
		
		Button button_3 = new Button("Indietro");
		verticalBox.add(button_3);
		button_3.addActionListener(this);
		
		Box verticalBox_1 = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox_1, BorderLayout.CENTER);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox);
		
		Label label = new Label("id Gita");
		horizontalBox.add(label);
		
		DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita dm = df.getDaoGita();
		textField = new TextField();
		textField.setText(String.valueOf(dm.getGite().size()+1));
		textField.setEditable(false);
		horizontalBox.add(textField);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_1);
		
		Label label_1 = new Label("Tipo");
		horizontalBox_1.add(label_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("MONTAGNA");
		horizontalBox_1.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.addActionListener(new radioAction());
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("MONGOLFIERA");
		horizontalBox_1.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.addActionListener(new radioAction2());
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_2);
		
		Label label_2 = new Label("Costo");
		horizontalBox_2.add(label_2);
		
		textField_1 = new TextField();
		horizontalBox_2.add(textField_1);
		
		Box horizontalBox_3 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_3);
		
		Label label_3 = new Label("num min");
		horizontalBox_3.add(label_3);
		
		textField_2 = new TextField();
		horizontalBox_3.add(textField_2);
		
		Box horizontalBox_4 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_4);
		
		Label label_4 = new Label("num max");
		horizontalBox_4.add(label_4);
		
		textField_3 = new TextField();
		horizontalBox_4.add(textField_3);
		
		Box horizontalBox_5 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_5);
		
		Label label_5 = new Label("data");
		horizontalBox_5.add(label_5);
		
		textField_4 = new TextField();
		horizontalBox_5.add(textField_4);
		
		Box horizontalBox_6 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_6);
		
		Label label_6 = new Label("Optional");
		horizontalBox_6.add(label_6);
		
		checkbox = new JCheckBox("Pranzo");
		horizontalBox_6.add(checkbox);
		
		checkbox_1 = new JCheckBox("Merenda");
		horizontalBox_6.add(checkbox_1);
		
		checkbox_2 = new JCheckBox("Visita a sito");
		horizontalBox_6.add(checkbox_2);
		
		Box horizontalBox_7 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_7);
		
		Label label_7 = new Label("Stato");
		horizontalBox_7.add(label_7);
		
		JRadioButton rdbtnNewRadioButton_6 = new JRadioButton("INCORSO");
		horizontalBox_7.add(rdbtnNewRadioButton_6);
		rdbtnNewRadioButton_6.addActionListener(new radioAction3());
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("TERMINATA");
		horizontalBox_7.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.addActionListener(new radioAction3());
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("DASVOLGERE");
		horizontalBox_7.add(rdbtnNewRadioButton_3);
		rdbtnNewRadioButton_3.addActionListener(new radioAction3());
		
		JRadioButton rdbtnNewRadioButton_4 = new JRadioButton("SOULDOUT");
		horizontalBox_7.add(rdbtnNewRadioButton_4);
		rdbtnNewRadioButton_4.addActionListener(new radioAction3());
		
		JRadioButton rdbtnNewRadioButton_5 = new JRadioButton("ANNULLATA");
		horizontalBox_7.add(rdbtnNewRadioButton_5);
		rdbtnNewRadioButton_5.addActionListener(new radioAction3());
		
		
	}

	
	public class radioAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			tipo = Tipo.MONTAGNA;
		}

	}
		
		public class radioAction2 implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				tipo = Tipo.MONGOLFIERA;
			}
			
		}
		
		public class radioAction3 implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				switch (e.getActionCommand()) {
				case "INCORSO":
					stato = Stato.INCORSO;
					break;
					
				case "DASVOLGERE":
					stato = Stato.DASVOLGERE;
					break;

				case "SOULDOUT":
					stato = Stato.SOLDOUT;
					break;

				case "TERMINATA":
					stato = Stato.TERMINATA;
					break;

				case "ANNULLATA":
					stato = Stato.ANNULLATA;
					break;


				default:
					break;
				}
			}
			
		}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getActionCommand().equals("Indietro")){
			try {
				frame.setVisible(false);
				HomeManagerVisualizzaGite a = new HomeManagerVisualizzaGite();
				a.main(null);
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoManager dm = df.getDaoManager();
		List<String> opt=new LinkedList<>();
		
		this.id= Integer.parseInt(textField.getText());
		this.data=LocalDate.parse(textField_4.getText());
		this.tipe = tipo.toString();
		this.minpart=Integer.parseInt(textField_2.getText());
		this.maxpart=Integer.parseInt(textField_3.getText());
		this.costo= Float.parseFloat(textField_1.getText());
		this.state= stato.toString();
		this.manager=dm.getIDManager(UserSingleton.getInstance().getManager().getUsername());
		if(checkbox.isSelected()==true)
			opt.add("Pranzo");
		if(checkbox_1.isSelected()==true)
			opt.add("Merenda");
		if(checkbox_2.isSelected()==true)
			opt.add("Visita a sito");
		this.optional=opt;
		List<Object> c = new LinkedList<>();
		Collections.addAll(c, id, data, tipe, minpart, maxpart, costo, state, manager, optional);
		
		switch (e.getActionCommand()) {
		case "Indietro":
			try {
				frame.setVisible(false);
				HomeManager a = new HomeManager();
				a.main(null);
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
			
		case "Aggiungi Gita":
			try {
				
					
					FrontController fc = new FrontController();
					fc.processRequest("AggiungiGita", c);
					c = new LinkedList<>();
					Collections.addAll(c, id, optional);
					fc.processRequest("AggiungiOptionalGita", c );
					
					MessageDisplayer.showErrorMessage("Gita Modificata");
					frame.setVisible(false);
					HomeManagerVisualizzaGite hmvg = new HomeManagerVisualizzaGite();
					hmvg.main(null);
				}
				
				 catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
			}
	}
}
