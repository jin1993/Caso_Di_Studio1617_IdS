package presentation.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;

import business.bo.Partecipante;
import business.bo.Prenotazione;
import business.bo.UserSingleton;
import business.to.GitaTO;
import business.to.PrenotazioneTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;
import presentation.FrontController;

import javax.swing.Box;
import java.awt.BorderLayout;
import java.awt.Button;

public class HomePartecipante implements ActionListener {

	private JFrame frmHomePartecipante;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePartecipante window = new HomePartecipante();
					window.frmHomePartecipante.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePartecipante() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHomePartecipante = new JFrame();
		frmHomePartecipante.setTitle("Home Partecipante");
		frmHomePartecipante.setBounds(100, 100, 666, 481);
		frmHomePartecipante.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frmHomePartecipante.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Visualizza Gite");
		verticalBox.add(button);
		button.addActionListener(this);
		
		Button button_1 = new Button("Visualizza Prenotazioni Effettuate");
		verticalBox.add(button_1);
		button_1.addActionListener(this);
		
		Button button_2 = new Button("Visualizza Dati Personali");
		verticalBox.add(button_2);
		button_2.addActionListener(this);
		
		Button button_3 = new Button("Logout");
		verticalBox.add(button_3);
		button_3.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Visualizza Gite":
			try {
				frmHomePartecipante.setVisible(false);
				visualizzagite();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
			
		case "Visualizza Prenotazioni Effettuate":
			try {
				frmHomePartecipante.setVisible(false);
				visualizzaprenotazioni();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		
		case "Visualizza Dati Personali":
			try {
				frmHomePartecipante.setVisible(false);
				visualizzadatipersonali();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		case "Logout":
			try {
				FrontController fc = new FrontController();
				fc.processRequest("EffettuaLogout", null);
				frmHomePartecipante.setVisible(false);
				LoginView l = new LoginView();
				l.main(null);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		}
	}

	private void visualizzadatipersonali() {
		DaoFactory daoFactory= DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoPartecipante dp = daoFactory.getDaoPartecipante();
		
		System.out.println("bastardo " + UserSingleton.getInstance().getPartecipante().getUsername());
		Partecipante p = new Partecipante(dp.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()));
		
		System.out.println("bastardo " + p.getNumtessanit());
		HomePartecipanteVisualizzaDatiPersonali hpvdp = new HomePartecipanteVisualizzaDatiPersonali();
		hpvdp.init(p);
		hpvdp.main(null);
		
	}

	private void visualizzaprenotazioni() {
		DaoFactory daoFactory= DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoPrenotazione dp = daoFactory.getDaoPrenotazione();
		DaoPartecipante dp1 = daoFactory.getDaoPartecipante();
		List<PrenotazioneTO> p = dp.getPrenotazioni(dp1.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()));
		frmHomePartecipante.setVisible(false);
		
		HomePartecipanteVisualizzaPrenotazioni hpvp = new HomePartecipanteVisualizzaPrenotazioni();
		hpvp.init(p);
		hpvp.main(null);
		
	}

	private void visualizzagite() {
		DaoFactory daoFactory= DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita dp = daoFactory.getDaoGita();
		DaoPartecipante dp1 = daoFactory.getDaoPartecipante();
		List<GitaTO> p = dp.getGite();
		frmHomePartecipante.setVisible(false);
		
		HomePartecipanteVisualizzaGita hpvp = new HomePartecipanteVisualizzaGita();
		hpvp.init(p);
		hpvp.main(null);
	}

}
