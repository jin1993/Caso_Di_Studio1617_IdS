package presentation.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.Box;
import java.awt.Component;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.awt.Panel;
import java.awt.Button;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import javax.swing.DropMode;
import javax.swing.JSeparator;

import business.bo.Manager;
import business.bo.Partecipante;
import business.bo.UserSingleton;
import business.casiduso.UserRole;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoManager;
import integration.interfaces.DaoPartecipante;
import presentation.FrontController;
import presentation.Logger;

public class LoginView implements ActionListener {

	private FrontController fc;
	private JFrame frmOutdoorsport;
	public TextField textField_1;
	public TextField textField;
	
	public boolean part = false;
	public boolean man = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginView window = new LoginView();
					window.frmOutdoorsport.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmOutdoorsport = new JFrame();
		frmOutdoorsport.setTitle("OutDoorSport");
		frmOutdoorsport.setBounds(100, 100, 450, 300);
		frmOutdoorsport.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		Label label_3 = new Label("Benvenuti nell'Applicazione \"OutDoorSport\"");
		label_3.setAlignment(Label.CENTER);
		frmOutdoorsport.getContentPane().add(label_3, BorderLayout.NORTH);
		
		Box verticalBox = Box.createVerticalBox();
		frmOutdoorsport.getContentPane().add(verticalBox, BorderLayout.CENTER);
		
		Label label = new Label("Login");
		verticalBox.add(label);
		label.setAlignment(Label.CENTER);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);
		
		Label label_1 = new Label("Username");
		horizontalBox.add(label_1);
		
		textField = new TextField();
		horizontalBox.add(textField);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_1);
		
		Label label_2 = new Label("Password");
		horizontalBox_1.add(label_2);
		
		textField_1 = new TextField();
		horizontalBox_1.add(textField_1);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Partecipante");
		rdbtnNewRadioButton_1.setSelected(true);
		verticalBox.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setSelected(false);
		rdbtnNewRadioButton_1.addActionListener(new radioAction());
		
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Manager");
		verticalBox.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton_1.setSelected(false);
		rdbtnNewRadioButton.addActionListener(new radioAction2());
		
		Button button = new Button("Login");
		verticalBox.add(button);
		
		Button button_1 = new Button("Close");
		verticalBox.add(button_1);
		
		Button button_2 = new Button("Registrati");
		verticalBox.add(button_2);
		button.addActionListener(this);
		button_1.addActionListener(this);
		button_2.addActionListener(this);
		
		JPanel panel = new JPanel();
		frmOutdoorsport.getContentPane().add(panel, BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Registrati":
			try {
				registra();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		case "Login":
			try {
				onEntra();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		case "Close":
			try {
				frmOutdoorsport.setVisible(false);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public class radioAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			part = true;
			man = false;
			
			DaoFactory dao = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoPartecipante dp = dao.getDaoPartecipante();
			System.out.println(dp.getPartecipante(textField.getText()).getUsername());
			UserSingleton.getInstance().setPartecipante(new Partecipante(dp.getPartecipante(textField.getText())));
			
		}
	}

	public class radioAction2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			man = true;
			part = false;
			
			DaoFactory dao = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoManager dp = dao.getDaoManager();
			
			UserSingleton.getInstance().setManager(new Manager(dp.getManager(textField.getText())));
		}
	}
	
	private void onEntra() throws Exception {
		if (textField.getText().equalsIgnoreCase("") || textField_1.getText().equalsIgnoreCase(""))
			MessageDisplayer.showErrorMessage("Compilare entrambi i campi");
		else {
			List<Object> parameter = new ArrayList<Object>();
			if(part==true){
				String backupUsername = UserSingleton.getInstance().getPartecipante().getUsername();
			}
			if(man==true){
				String backupUsername = UserSingleton.getInstance().getManager().getUsername();
			}
			UserRole backupRole = Logger.getInstance().getUserRole();
			String username = textField.getText();
			String password = textField_1.getText();
			Collections.addAll(parameter, username, password);
			
			frmOutdoorsport.setVisible(false);
			FrontController frontController = new FrontController();
			frontController.processRequest("EffettuaLogin", parameter);
			if(part == true) {
				HomePartecipante hp = new HomePartecipante();
				hp.main(null);
			}
			if(man == true) {
				HomeManager hp = new HomeManager();
				hp.main(null);
			}
				
		}
	}
	
	public void registra() throws Exception {
		if (part == true) {
			frmOutdoorsport.setVisible(false);
			FrontController frontController = new FrontController();
			RegistrazioneView r = new RegistrazioneView();
			r.main(null);
			
		} else
			MessageDisplayer.showErrorMessage("Funzione ammessa solo partecipanti...");
	}
}
