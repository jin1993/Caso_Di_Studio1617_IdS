package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JFrame;
import java.awt.TextArea;
import javax.swing.JTable;
import javax.swing.JTextArea;

import business.bo.Optional;
import business.bo.Prenotazione;
import business.bo.UserSingleton;
import business.to.PrenotazioneTO;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;
import presentation.FrontController;
import javax.swing.JCheckBox;

public class HomePartecipanteDettagliPrenotazione implements ActionListener{

	private ArrayList op;
	private PrenotazioneTO p;
	private JCheckBox checkbox_2;
	private Optional o;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePartecipanteDettagliPrenotazione window = new HomePartecipanteDettagliPrenotazione();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePartecipanteDettagliPrenotazione() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 641, 440);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button_2 = new Button("Modifica Prenotazione");
		verticalBox.add(button_2);
		button_2.addActionListener(this);
		
		Button button_3 = new Button("Indietro");
		button_3.addActionListener(this);
		verticalBox.add(button_3);
		
		Panel panel = new Panel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		Box verticalBox_1 = Box.createVerticalBox();
		panel.add(verticalBox_1);
		
		Label label = new Label("Scegli Optional");
		verticalBox_1.add(label);
		
		Panel panel_1 = new Panel();
		verticalBox_1.add(panel_1);
		
		/*for(int i = 0; (this.p.getOptional()).size()>i; i++) {
			checkbox_2 = new Checkbox(this.p.getOptional().get(i).getNome());
			panel_1.add(checkbox_2);
		}*/
		
		JCheckBox checkbox_2 = new JCheckBox("Merenda");
		panel_1.add(checkbox_2);
		
		JCheckBox checkbox_1 = new JCheckBox("Pranzo");
		panel_1.add(checkbox_1);
		
		JCheckBox checkbox = new JCheckBox("Visita al sito");
		panel_1.add(checkbox);
		
		Panel panel_2 = new Panel();
		verticalBox_1.add(panel_2);
		
		JTextArea textArea = new JTextArea();
		panel_2.add(textArea);
		
		TextArea textArea_1 = new TextArea();
		panel_2.add(textArea_1);
		
		DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional dop = df.getDaoOptional();
		DaoGita dg = df.getDaoGita();
		DaoPartecipante da=df.getDaoPartecipante();
		DaoPrenotazione dp=df.getDaoPrenotazione();
		
		System.out.println("GIta "+UserSingleton.getInstance().getPartecipante().getUsername());
		p=dp.getPrenotazione(da.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()), dg.getGita(UserSingleton.getInstance().getGcorrente().getId()));
		
		
		textArea_1.setText("Gita: " + p.getGita().getTipo().toString());
		textArea_1.setText("Data Gita: " + p.getGita().getData().toString());
		textArea_1.setText("Costo Gita: " + p.getGita().getCosto());
		textArea_1.setText("Optional Scelti: " );
		for(int i = 0; (p.getOptional()).size()>i ; i++)
		{
			textArea_1.setText("Nome Optional: " + p.getOptional().get(i).getNome());
			textArea_1.setText("Costo Optional: " + p.getOptional().get(i).getCosto());
		}
		
		FrontController fcController = new FrontController();
		try {
			List<Object> l = new LinkedList<Object>();
			l.add(UserSingleton.getInstance().getPartecipante().getUsername());
			l.add(p.getGita().getId());
			fcController.processRequest("CalcolaCostoFinale",l);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			List<Object> l = new LinkedList<Object>();
			l.add(UserSingleton.getInstance().getPartecipante().getUsername());
			l.add(p.getGita().getId());
			textArea_1.setText("COSTO TOTALE: " + 
					fcController.processRequest("CalcolaCostoFinale", l));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	public void init(PrenotazioneTO prenotazioneTO) {
		this.p = prenotazioneTO;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Indietro":
			try {
				frame.setVisible(false);
				HomePartecipanteVisualizzaPrenotazioni a = new HomePartecipanteVisualizzaPrenotazioni();
				a.main(null);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
			
		case "Modifica Prenotazione":
			try {
				if(checkbox_2.isSelected()!=false) {
					List<Object> par = null;
					DaoFactory df = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
					DaoOptional dop = df.getDaoOptional();
					par.add(p);
					par.add(dop.getOptional(checkbox_2.getName()));
					FrontController fc = new FrontController();
					fc.processRequest("ModificaOptionalPrenotazione", par);
					HomePartecipanteDettagliPrenotazione hpdp = new HomePartecipanteDettagliPrenotazione();
					hpdp.init(p);
					hpdp.main(null);
				}
				
				
				frame.setVisible(false);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		
		}
	}

}
