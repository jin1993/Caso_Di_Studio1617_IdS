package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.JFrame;

import business.bo.UserSingleton;
import business.to.GitaTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoManager;
import integration.interfaces.DaoPartecipante;
import presentation.FrontController;

public class HomeManager implements ActionListener{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeManager window = new HomeManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomeManager() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Visualizza Gite Inserite");
		verticalBox.add(button);
		button.addActionListener(this);
		
		Button button_1 = new Button("Aggiungi Nuova Gita");
		verticalBox.add(button_1);
		button_1.addActionListener(this);
		
		Button button_3 = new Button("Logout");
		verticalBox.add(button_3);
		button_3.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Visualizza Gite Inserite":
			try {
				frame.setVisible(false);
				visualizzagite();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
			
		case "Aggiungi Nuova Gita":
			try {
				frame.setVisible(false);
				aggiungigita();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;
		case "Logout":
			try {
				FrontController fc = new FrontController();
				fc.processRequest("EffettuaLogout", null);
				LoginView l = new LoginView();
				l.main(null);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		}
	}
	
	private void aggiungigita() {
		HomeManagerAggiungiGita hmag = new HomeManagerAggiungiGita();
		hmag.main(null);
	}

	private void visualizzagite() {
		DaoFactory daoFactory= DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita dp = daoFactory.getDaoGita();
		DaoManager dm = daoFactory.getDaoManager();
		DaoPartecipante dp1 = daoFactory.getDaoPartecipante();
		List<GitaTO> p = dm.getGite(dm.getManager(UserSingleton.getInstance().getManager().getUsername()));
		
		HomeManagerVisualizzaGite hpvp = new HomeManagerVisualizzaGite();
		hpvp.init(p);
		hpvp.main(null);
		
	}
	
	
	

}
