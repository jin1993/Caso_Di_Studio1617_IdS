package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import java.awt.TextArea;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JList;

import business.bo.Gita;
import business.bo.Partecipante;
import business.bo.UserSingleton;
import business.to.*;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;
import presentation.FrontController;

public class HomePartecipanteVisualizzaPrenotazioni implements ActionListener {

	private List<PrenotazioneTO> par ;
	private int pos = -1;
	private JFrame frame;
	JComboBox<String> c ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePartecipanteVisualizzaPrenotazioni window = new HomePartecipanteVisualizzaPrenotazioni();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePartecipanteVisualizzaPrenotazioni() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 642, 447);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Cancella Prenotazione");
		verticalBox.add(button);
		button.addActionListener(this);
		
		Button button_3 = new Button("Indietro");
		button_3.addActionListener(this);
		
		
		Button button_2 = new Button("Dettagli Prenotazione");
		verticalBox.add(button_2);
		verticalBox.add(button_3);
		
		button_2.addActionListener(this);
		
		//java.awt.List list = new java.awt.List();
		
		
		DaoFactory d = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita dg = d.getDaoGita();
		DaoPrenotazione dp = d.getDaoPrenotazione();
		DaoPartecipante dp1 = d.getDaoPartecipante();
		Partecipante p = new Partecipante(dp1.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()));
		par = dp.getPrenotazioni(dp1.getPartecipante(UserSingleton.getInstance().getPartecipante().getUsername()));
		/*for(int i = 0; par.size()>i;i++) {
			list.add("Gita: " + dg.getGita(par.get(i).getGita().getId()).getTipo().toString() +
					", Data Gita: " + dg.getGita(par.get(i).getGita().getId()).getData().toString() +
					", Data Prenotazione: " + par.get(i).getDataprenotazione().toString());
		}*/
		
		
		
		c = new JComboBox<String>();
		for(int i = 0; par.size()>i;i++) {
			//while(!g.get(i).getStato().equals(Stato.TERMINATA)) {
				c.addItem("Gita: " +dg.getGita(par.get(i).getGita().getId()).getTipo().toString() +
						",  Data Gita: " + dg.getGita(par.get(i).getGita().getId()).getData().toString() +
						", Data Prenotazione: " + par.get(i).getDataprenotazione().toString());
			//}
		}
		frame.getContentPane().add(c, BorderLayout.CENTER);
		c.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pos = c.getSelectedIndex();
				UserSingleton.getInstance().setGcorrente( new Gita(dg.getGita(par.get(pos).getGita().getId())));
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(pos);
		if(pos > -1 && pos<par.size()) {
			switch (e.getActionCommand()) {
			case "Cancella Prenotazione":
				try {
					FrontController fc = new FrontController();
					List<Object> l = new LinkedList<Object>();
					l.add(UserSingleton.getInstance().getGcorrente().getId());
					l.add(UserSingleton.getInstance().getPartecipante().getUsername());
					fc.processRequest("CancellaPrenotazione", l);
					MessageDisplayer.showErrorMessage("Prenotazione Cancellata!");
					frame.setVisible(false);
					HomePartecipanteVisualizzaPrenotazioni a = new HomePartecipanteVisualizzaPrenotazioni();
					a.main(null);
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				break;
				
			case "Dettagli Prenotazione":
				try {
					frame.setVisible(false);
					HomePartecipanteDettagliPrenotazione hpdp = new HomePartecipanteDettagliPrenotazione();
					hpdp.init(par.get(pos));
					hpdp.main(null);
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				break;
				
			
			}
		if(e.getActionCommand().equals("Indietro")) {
			frame.setVisible(false);
			HomePartecipante hp = new HomePartecipante();
			hp.main(null);
		}
		}
		
	}
	
	public void init(List<PrenotazioneTO> par) {
		this.par=par;
	}

}
