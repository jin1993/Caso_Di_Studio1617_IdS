package presentation.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.ComboBoxEditor;
import javax.swing.JFrame;
import java.awt.Panel;
import java.awt.Choice;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTextField;

import business.bo.Gita;
import business.bo.Partecipante;
import business.bo.UserSingleton;
import business.to.GitaTO;
import business.to.Stato;
import errorhandling.MessageDisplayer;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;
import presentation.FrontController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomePartecipanteVisualizzaGita implements ActionListener{

	private List<GitaTO> g = null;
	private JFrame frame;
	JTextField t;
	GitaTO gita = null;
	JComboBox<String> c ;
	private int pos = -1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePartecipanteVisualizzaGita window = new HomePartecipanteVisualizzaGita();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePartecipanteVisualizzaGita() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 666, 481);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box verticalBox = Box.createVerticalBox();
		frame.getContentPane().add(verticalBox, BorderLayout.EAST);
		
		Button button = new Button("Prenota Gita");
		verticalBox.add(button);
		button.addActionListener(this);
		
		Button button_3 = new Button("Indietro");
		button_3.addActionListener(this);
		verticalBox.add(button_3);
		
		//java.awt.List list = new java.awt.List();
		//frame.getContentPane().add(list, BorderLayout.CENTER);
		
		DaoFactory d = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita dg = d.getDaoGita();
		g = dg.getGite();
		c = new JComboBox<String>();
		for(int i = 0; g.size()>i;i++) {
			//while(!g.get(i).getStato().equals(Stato.TERMINATA)) {
				c.addItem("Gita: " + g.get(i).getId() +
						", Tipo : " + g.get(i).getTipo().toString() +
						", Data : " + g.get(i).getData().toString() +
						", Stato : "+ g.get(i).getStato().toString() +
						", Costo: " + g.get(i).getCosto());
			//}
		}
		
		frame.getContentPane().add(c, BorderLayout.CENTER);
		c.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pos = c.getSelectedIndex();
				UserSingleton.getInstance().setGcorrente( new Gita(g.get(pos)));
			}
		});
		
		
	}

	public void init(java.util.List<GitaTO> p) {
		this.g=p;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(pos > -1 && pos<g.size()) {
			switch (e.getActionCommand()) {
			case "Prenota Gita":
				try {
					frame.setVisible(false);
					HomePartecipantePrenotazioneGita a = new HomePartecipantePrenotazioneGita();
					a.init(gita);
					a.main(null);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				break;
			
			}
		if(e.getActionCommand().equals("Indietro")) {
			HomePartecipante hp = new HomePartecipante();

			frame.setVisible(false);
			hp.main(null);
		}
		
		}
	}

}
