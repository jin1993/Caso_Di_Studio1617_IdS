package presentation;

import java.util.List;

public class FrontController {
	
	private ApplicationControllerInt applicationController;

	public FrontController() {
		this.applicationController = new ApplicationController();
	}
	
	public Object processRequest(String request, List<Object> parameters) throws Exception
	{
		return applicationController.handleRequest(request, parameters);
	}

}
