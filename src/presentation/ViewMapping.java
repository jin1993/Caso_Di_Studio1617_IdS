package presentation;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import business.casiduso.UserRole;

public class ViewMapping {
	
	public static final String startView = ""; //TODO: definire un fxml della schermata iniziale
	
	public static Map<UserRole, String> homeView = new EnumMap<UserRole, String>(UserRole.class);
	public static Map<String, Class> viewStringToControllerClass = new HashMap<String, Class>();
	
	static
	{
		homeView.put(UserRole.ANONIMO, "/presentation/views/LoginView.java");
		homeView.put(UserRole.PARTECIPANTE, "/presentation/views/HomePartecipante.java");
		homeView.put(UserRole.MANAGER, "/presentation/views/HomeManager.java");
	}
	

}
