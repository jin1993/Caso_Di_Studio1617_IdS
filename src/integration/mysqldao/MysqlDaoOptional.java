package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.plaf.OptionPaneUI;

import business.to.ManagerTO;
import business.to.OptionalTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoOptional;

public class MysqlDaoOptional implements DaoOptional {

	@Override
	public OptionalTO getOptional(int id) throws DaoException {
		OptionalTO optional = null;
		String query = "SELECT * FROM Optional WHERE idOptional = ?;";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, id);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				optional = new OptionalTO();

				optional.setData(results.getInt("idOptional"), results.getString("Nome"),  results.getFloat("Costo"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return optional;

	}

	@Override
	public OptionalTO getOptional(String nome) throws DaoException {
		OptionalTO optional = null;
		String query = "SELECT * FROM Optional WHERE Nome = ?;";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, nome);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				optional = new OptionalTO();

				optional.setData(results.getInt("idOptional"), results.getString("Nome"), results.getFloat("Costo"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return optional;


	}

	@Override
	public List<OptionalTO> getOptional() throws DaoException {
		String query = "Select * from Optional";
		Connection con = null;
		PreparedStatement statement = null;
		List<OptionalTO> listOpt = new LinkedList<OptionalTO>();

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				OptionalTO optionalTO = new OptionalTO();

				optionalTO.setData(results.getInt("idOptional"), results.getString("Nome"), results.getFloat("Costo"));
				listOpt.add(optionalTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;
	}

	@Override
	public List<OptionalTO> getOptional(OptionalTO optional) throws DaoException {
		String query = createQueryString("SELECT *", optional);
		Connection con = null;
		PreparedStatement statement = null;
		List<OptionalTO> listOpt = new LinkedList<OptionalTO>();

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				OptionalTO optionalTO = new OptionalTO();

				optionalTO.setData(results.getInt("idOptional"), results.getString("Nome"), results.getFloat("Costo"));

				listOpt.add(optionalTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;
	}

	@Override
	public void setOptional(OptionalTO optional) throws DaoException {
		String query = "INSERT INTO Optional (idOptional, Nome, Costo) VALUES (?, ?, ?)";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, optional.getId());
			statement.setString(2, optional.getNome());
			statement.setFloat(3, optional.getCosto());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}


	}

	@Override
	public void updateOptional(OptionalTO optional) throws DaoException {
		String query = "UPDATE Optional SET nome = ?, costo = ? WHERE idOptional = ?";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, optional.getNome());
			statement.setDouble(2, optional.getCosto());
			statement.setInt(3, optional.getId());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteOptional(OptionalTO optional) throws DaoException {
		String query = createQueryString("DELETE", optional);
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	private String createQueryString(String beginString, OptionalTO opto)
	{

		String query = beginString + " from Optional where ";

		if(opto.getId() != 0)
			query += "Optional.idOptional = " + opto.getId() + " AND ";
		if(opto.getNome() != null)
			query += "Optional.nome = '" + opto.getNome() + "' AND ";
		if(opto.getCosto() != 0) 
			query += "Optional.costo = " + opto.getCosto() + " AND ";


		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}
}
