package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import business.to.PartecipanteTO;
import business.to.Sesso;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoPartecipante;

public class MysqlDaoPartecipante implements DaoPartecipante {

	@Override
	public PartecipanteTO getPartecipanteByEmail(String email) {
		PartecipanteTO parTO = null;
		Connection con = null;
		String query = "SELECT * from Partecipante where Email = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, email);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				parTO = new PartecipanteTO();
				parTO.setUsername(results.getString("Username"));
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setCodf(results.getString("CF"));
				parTO.setSesso(Enum.valueOf(Sesso.class, results.getString("Sesso")));
				parTO.setVia(results.getString("Via"));
				parTO.setNcivico(results.getString("Ncivico"));
				parTO.setCitta(results.getString("Citta"));
				parTO.setNumtessanit(results.getInt("N_tesseras"));
				parTO.setCertificato(results.getString("Certificato"));
				parTO.setDatacert( results.getDate("Data_certificato").toLocalDate());
				parTO.setDatanascita(results.getDate("Anno_nascita").toLocalDate());
				parTO.setEmail(email);
				
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return parTO;
	}

	@Override
	public PartecipanteTO getPartecipante(String username) {
		PartecipanteTO parTO = null;
		Connection con = null;
		String query = "SELECT * from Partecipante where Username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, username);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				parTO = new PartecipanteTO();
				parTO.setUsername(username);
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setCodf(results.getString("CF"));
				parTO.setSesso(Enum.valueOf(Sesso.class, results.getString("Sesso")));
				parTO.setVia(results.getString("Via"));
				parTO.setNcivico(results.getString("Ncivico"));
				parTO.setCitta(results.getString("Citta"));
				parTO.setNumtessanit(results.getInt("N_tesseras"));
				parTO.setCertificato(results.getString("Certificato"));
				parTO.setDatacert( results.getDate("Data_certificato").toLocalDate());
				parTO.setDatanascita(results.getDate("Anno_nascita").toLocalDate());
				parTO.setEmail(results.getString("Email"));
				
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return parTO;
	}

	@Override
	public List<PartecipanteTO> getPartecipante() {
		List<PartecipanteTO> ParTO = null;
		Connection con = null;
		String query = "SELECT * from Partecipante;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				PartecipanteTO parTO =  new PartecipanteTO();
				parTO.setUsername(results.getString("Username"));
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setCodf(results.getString("CF"));
				parTO.setSesso(Enum.valueOf(Sesso.class, results.getString("Sesso")));
				parTO.setVia(results.getString("Via"));
				parTO.setNcivico(results.getString("Ncivico"));
				parTO.setCitta(results.getString("Citta"));
				parTO.setNumtessanit(results.getInt("N_tesseras"));
				parTO.setCertificato(results.getString("Certificato"));
				parTO.setDatacert( results.getDate("Data_certificato").toLocalDate());
				parTO.setDatanascita(results.getDate("Anno_nascita").toLocalDate());
				parTO.setEmail(results.getString("Email"));
				
				ParTO.add(parTO);
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return  ParTO;
	}

	@Override
	public List<PartecipanteTO> getPartecipante(PartecipanteTO partecipante) {
		List<PartecipanteTO> ParTO = null;
		Connection con = null;
		String query = createQueryString("SELECT * ",partecipante);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				PartecipanteTO parTO =  new PartecipanteTO();
				parTO.setUsername(results.getString("Username"));
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setCodf(results.getString("CF"));
				parTO.setSesso(Enum.valueOf(Sesso.class, results.getString("Sesso")));
				parTO.setVia(results.getString("Via"));
				parTO.setNcivico(results.getString("Ncivico"));
				parTO.setCitta(results.getString("Citta"));
				parTO.setNumtessanit(results.getInt("N_tesseras"));
				parTO.setCertificato(results.getString("Certificato"));
				parTO.setDatacert( results.getDate("Data_certificato").toLocalDate());
				parTO.setDatanascita(results.getDate("Anno_nascita").toLocalDate());
				parTO.setEmail(results.getString("Email"));
				
				ParTO.add(parTO);
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return  ParTO;
	}
	

	@Override
	public void setPartecipante(PartecipanteTO part) {
		Connection con = null;
		String query = "INSERT INTO Partecipante VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, part.getUsername());
			statement.setString(2, part.getNome());
			statement.setString(3, part.getCognome());
			statement.setString(4, part.getCodf());
			statement.setString(5, part.getSesso().toString());
			statement.setString(6, part.getVia());
			statement.setString(7, part.getNcivico());
			statement.setString(8, part.getCitta());
			statement.setInt(9, part.getNumtessanit());
			statement.setString(10, part.getCertificato());
			statement.setDate(11, java.sql.Date.valueOf(part.getDatacert()));
			statement.setDate(12, java.sql.Date.valueOf(part.getDatanascita()));
			statement.setString(13, part.getEmail());

			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		
	}

	@Override
	public void deletePartecipante(PartecipanteTO partecipante) {
		Connection con = null;
		String query = createQueryString("DELETE", partecipante);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}


	@Override
	public void updatePartecipante(PartecipanteTO part) {
		Connection con = null;
		String query = "UPDATE Partecipante SET Username = ?, Nome = ?, Cognome = ?, CF = ?, Sesso = ?, Via =?, "
				+ "Ncivico=?, Citta=?, Numtessanit=?, Certificato=?, Datacert =?, Datanascita=?, Email=?  WHERE Username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, part.getUsername());
			statement.setString(2, part.getNome());
			statement.setString(3, part.getCognome());
			statement.setString(4, part.getCodf());
			statement.setString(5, part.getSesso().toString());
			statement.setString(6, part.getVia());
			statement.setString(7, part.getNcivico());
			statement.setString(8, part.getCitta());
			statement.setInt(9, part.getNumtessanit());
			statement.setString(10, part.getCertificato());
			statement.setDate(11, java.sql.Date.valueOf(part.getDatacert()));
			statement.setDate(12, java.sql.Date.valueOf(part.getDatanascita()));
			statement.setString(13, part.getEmail());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	
	private String createQueryString(String beginString, PartecipanteTO par)
	{
		String query = beginString + " from Partecipante where ";
		
		if(par.getUsername() != null)
			query += "Partecipante.Username = '" + par.getUsername() + "' AND ";
		if(par.getNome() != null)
			query += "Partecipante.Nome = '" + par.getNome() + "' AND ";
		if(par.getCognome() != null)
			query += "Partecipante.Cognome = '" + par.getCognome() + "' AND ";
		if(par.getCodf() != null)
			query += "Partecipante.CF = '" + par.getCodf() + "' AND ";
		if(par.getSesso() != null)
			query += "Partecipante.Sesso = '" + par.getSesso().toString() + "' AND ";
		if(par.getVia() != null)
			query += "Partecipante.Via = '" + par.getVia() + "' AND ";
		if(par.getNcivico() != null)
			query += "Partecipante.Ncivico = '" + par.getNcivico() + "' AND ";
		if(par.getCitta() != null)
			query += "Partecipante.Citta = '" + par.getCitta() + "' AND ";
		if(par.getNumtessanit() != 0)
			query += "Partecipante.Numtessanit = '" + par.getNumtessanit() + "' AND ";
		if(par.getCertificato() != null)
			query += "Partecipante.Certificato = '" + par.getCertificato() + "' AND ";
		if(par.getDatacert() != null)
			query += "Partecipante.Datacert = '" + par.getDatacert() + "' AND ";
		if(par.getDatanascita()!= null)
			query += "Partecipante.Datanascita = '" + par.getDatanascita() + "' AND ";
		if(par.getEmail() != null)
			query += "Partecipante.Email = '" + par.getEmail() + "' AND ";
			
		query = query.substring(0, query.length() - 5);
		query += ";";
		
		return query;
	}
}
