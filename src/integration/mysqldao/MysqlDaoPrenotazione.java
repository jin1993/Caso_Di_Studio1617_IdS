package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import business.bo.Gita;
import business.bo.Optional;
import business.bo.Partecipante;
import business.to.GitaTO;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import business.to.PrenotazioneTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoPrenotazione;

public class MysqlDaoPrenotazione implements DaoPrenotazione {


	@Override
	public List<PrenotazioneTO> getPrenotazioni() {
		List<PrenotazioneTO> LpreTO = new LinkedList<PrenotazioneTO>();
		Connection con = null;
		String query = "SELECT * from Prenotazione;";
		
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
						
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				PrenotazioneTO pTO= new PrenotazioneTO();
				MysqlDaoPartecipante par= new MysqlDaoPartecipante();
				MysqlDaoGita gita = new MysqlDaoGita();
				pTO.setPartecipante(new Partecipante(par.getPartecipante(results.getString("Partecipante_usernamePartecipante"))));
				pTO.setGita(new Gita(gita.getGita(results.getInt("Gita_idGita"))));
				pTO.setDataprenotazione(results.getDate("Data_Prenotazione").toLocalDate());

				
				LpreTO.add(pTO);
			}
			
			if(LpreTO.isEmpty())
				throw new NoEntryException("Non ci sono prenotazioni");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LpreTO;
		
	}

	@Override
	public List<PartecipanteTO> getPrenotazioni(GitaTO gita) {
		List<PartecipanteTO> LparTO = new LinkedList<>();
		Connection con = null;
		String query = "SELECT * from Prenotazione where Gita_idGita = ?;";
		
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, gita.getId());
						
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				PartecipanteTO parTO= new PartecipanteTO();
				MysqlDaoPartecipante par= new MysqlDaoPartecipante();
				parTO = par.getPartecipante(results.getString("Partecipante_usernamePartecipante"));
				
				LparTO.add(parTO);
			}
			if(LparTO.isEmpty())
				LparTO = null;
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LparTO;
		
	}

	@Override
	public List<OptionalTO> getOptionalPrenotazione(PrenotazioneTO prenotazione) {
		String query = "SELECT * from (Prenotazione inner join OptionalSceltiDaPart on Prenotazione.Gita_idGita=OptionalSceltiDaPart.Op_Gita_idGita)"
				+ "inner join Optional on  OptionalSceltiDaPart.Op_Gita_Optional_idOptional=Optional.idOptional "
				+ "where Prenotazione.Partecipante_usernamePartecipante= ?";
		Connection con = null;
		PreparedStatement statement = null;
		List<OptionalTO> listOpt = new LinkedList<OptionalTO>();

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, prenotazione.getPartecipante().getUsername());

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				OptionalTO optionalTO = new OptionalTO();

				optionalTO.setData(results.getInt("Optional.idOptional"), results.getString("Optional.Nome"), results.getFloat("Optional.Costo"));

				listOpt.add(optionalTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;
	}

	@Override
	public void setPrenotazione(PrenotazioneTO prenotazione) {
		Connection con = null;
		String query = "INSERT INTO Prenotazione VALUES (?, ?, ?);";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, prenotazione.getPartecipante().getUsername());
			statement.setInt(2, prenotazione.getGita().getId());
			statement.setDate(3, java.sql.Date.valueOf(prenotazione.getDataprenotazione()));

			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		
	}

	@Override
	public void deletePrenotazione(PrenotazioneTO prenotazione) {
		Connection con = null;
		String query = "DELETE from Prenotazione where partecipante_usernamepartecipante=?, gita_idgita=?, data_prenotazione=?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, prenotazione.getPartecipante().getUsername());
			statement.setInt(2, prenotazione.getGita().getId());
			statement.setDate(3, java.sql.Date.valueOf(prenotazione.getDataprenotazione()));
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updatePrenotazione(PrenotazioneTO prenotazione) {
		Connection con = null;
		String query = "UPDATE Prenotazione SET Gita_idGita=?, Data_Prenotazione=? WHERE Partecipante_usernamePartecipante=?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, prenotazione.getGita().getId());
			statement.setDate(2, java.sql.Date.valueOf(prenotazione.getDataprenotazione()));
			statement.setString(3, prenotazione.getPartecipante().getUsername());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	@Override
	public List<PrenotazioneTO> getPrenotazioni(PartecipanteTO partecipante) {
		List<PrenotazioneTO> LpreTO = new LinkedList<PrenotazioneTO>();
		Connection con = null;
		String query = "SELECT * from Prenotazione where Partecipante_usernamePartecipante = ?;";
		
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, partecipante.getUsername());
						
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				PrenotazioneTO pTO= new PrenotazioneTO();
				MysqlDaoPartecipante par= new MysqlDaoPartecipante();
				MysqlDaoGita gita = new MysqlDaoGita();
				pTO.setPartecipante(new Partecipante(partecipante));
				pTO.setGita(new Gita(gita.getGita(results.getInt("Gita_idGita"))));
				pTO.setDataprenotazione(results.getDate("Data_Prenotazione").toLocalDate());
				
				LpreTO.add(pTO);
			}
			if(LpreTO.isEmpty())
				throw new NoEntryException("Non ci sono prenotazioni");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LpreTO;
		
	}

	@Override
	public List<PrenotazioneTO> getPrenotazioni(LocalDate data) {
		List<PrenotazioneTO> LpreTO = null;
		Connection con = null;
		String query = "SELECT * from Prenotazione where Data_Prenotazione = ?;";
		
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setDate(1, java.sql.Date.valueOf(data));
						
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				PrenotazioneTO pTO= new PrenotazioneTO();
				MysqlDaoPartecipante par= new MysqlDaoPartecipante();
				MysqlDaoGita gita = new MysqlDaoGita();
				pTO.setPartecipante(new Partecipante(par.getPartecipante(results.getString("Partecipante_usernamePartecipante"))));
				pTO.setGita(new Gita(gita.getGita(results.getInt("Gita_idGita"))));
				pTO.setDataprenotazione(data);
				
				LpreTO.add(pTO);
			}
			if(LpreTO.isEmpty())
				throw new NoEntryException("Non ci sono prenotazioni");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LpreTO;
	}

	@Override
	public void setOptionalPrenotazione(PrenotazioneTO prenotazione, List<OptionalTO> optional) {
		List<Optional> Listopgita = prenotazione.getGita().getOptional();
		Connection con = null;
		PreparedStatement statement = null;
		
		try {
			con = MysqlDaoFactory.getConnection();
			if(Listopgita != null && Listopgita.contains(optional)) {
				
				for(int i = 0; optional.size()>i; i++) {
					String query = "INSERT INTO OptionalSceltiDaPar VALUES (?, ?, ?);";
					statement = con.prepareStatement(query);
					
					statement.setInt(1, prenotazione.getGita().getId());
					statement.setInt(2, optional.get(i).getId());
					statement.setString(3, prenotazione.getPartecipante().getUsername());
					
					statement.executeUpdate();
				}
			}
		}catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public PrenotazioneTO getPrenotazione(PartecipanteTO partecipante, GitaTO gita) {
		Connection con = null;
		String query = "SELECT * from Prenotazione where Partecipante_usernamePartecipante ='?', Gita_idGita=? ;";
		
		PreparedStatement statement = null;
		PrenotazioneTO pTO= new PrenotazioneTO();
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, partecipante.getUsername());
			statement.setInt(2, gita.getId());
						
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				
				MysqlDaoPartecipante par= new MysqlDaoPartecipante();
				MysqlDaoGita g = new MysqlDaoGita();
				pTO.setPartecipante(new Partecipante(partecipante));
				pTO.setGita(new Gita(g.getGita(results.getInt("Gita_idGita"))));
				pTO.setDataprenotazione(results.getDate("Data_Prenotazione").toLocalDate());
				
			}
			else
				throw new NoEntryException("Non ci sono prenotazioni");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return pTO;
	}

	@Override
	public void deleteOptionalPrenotazione(PrenotazioneTO prenotazione) {
		List<OptionalTO> Listoppre = getOptionalPrenotazione(prenotazione);
		Connection con = null;
		PreparedStatement statement = null;
		
		try {
			con = MysqlDaoFactory.getConnection();
			if(Listoppre != null) {
				
				for(int i = 0; Listoppre.size()>i; i++) {
					String query = "DELETE FROM OptionalSceltiDaPar where Op_Gita_Gita_idGita=?, Op_Gita_Optional_idOptional=?, Partecipante_usernamePartecipante=?;";
					statement = con.prepareStatement(query);
					
					statement.setInt(1, prenotazione.getGita().getId());
					statement.setInt(2, Listoppre.get(i).getId());
					statement.setString(3, prenotazione.getPartecipante().getUsername());
					
					statement.executeUpdate();
				}
			}
		}catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		
	}

	@Override
	public void updateOptionalPrenotazione(PrenotazioneTO prenotazione, List<OptionalTO> optional) {
		
		deleteOptionalPrenotazione(prenotazione);
		
		List<Optional> Listopgita = prenotazione.getGita().getOptional();
		Connection con = null;
		PreparedStatement statement = null;
		
		try {
			con = MysqlDaoFactory.getConnection();
			if(Listopgita != null && Listopgita.contains(optional)) {
				
				for(int i = 0; optional.size()>i; i++) {
					String query = "Update OptionalSceltiDaPar set Op_Gita_Gita_idGita = ?, Op_Gita_Optional_idOptional = ?, Partecipante_usernamePartecipante = ?);";
					statement = con.prepareStatement(query);
					
					statement.setInt(1, prenotazione.getGita().getId());
					statement.setInt(2, optional.get(i).getId());
					statement.setString(3, prenotazione.getPartecipante().getUsername());
					
					statement.executeUpdate();
				}
			}
		}catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}
}

