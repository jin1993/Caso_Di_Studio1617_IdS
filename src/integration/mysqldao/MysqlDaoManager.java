package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.bo.Manager;
import business.to.GitaTO;
import business.to.ManagerTO;
import business.to.PartecipanteTO;
import business.to.Sesso;
import business.to.Tipo;
import business.to.Stato;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoManager;

public class MysqlDaoManager implements DaoManager {

	@Override
	public ManagerTO getManager(String username) throws DaoException, NoEntryException {
		ManagerTO manTO = null;
		Connection con = null;
		String query = "SELECT * from Manager where username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, username);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				manTO = new ManagerTO();
				manTO.setNome(results.getString("Nome"));
				manTO.setCognome(results.getString("Cognome"));
				manTO.setEmail(results.getString("Email"));
				manTO.setUsername(results.getString("username"));
				
			}
			else
				throw new NoEntryException("Non ci sono manager con il nome indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return manTO;
	}

	@Override
	public int getIDManager(String username) throws DaoException, NoEntryException {
		int id;
		Connection con = null;
		String query = "SELECT idManager from Manager where username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, username);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				id=results.getInt("idManager");
						}
			else
				throw new NoEntryException("Non ci sono manager con il nome indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return id;
	}

	@Override
	public List<GitaTO> getGite(ManagerTO manager) throws DaoException, NoEntryException {
		List<GitaTO> TOg = new LinkedList<>();
		Connection con = null;
		String query = "SELECT * from Gita where idManager = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, getIDManager(manager.getUsername()));
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				GitaTO gTO = new GitaTO();
				gTO.setId(results.getInt("idGita"));
				gTO.setData(results.getDate("Data").toLocalDate());
				gTO.setTipo(Enum.valueOf(Tipo.class, results.getString("Tipo")));
				gTO.setMinpart(results.getInt("Nmin"));
				gTO.setMaxpart(results.getInt("Nmax"));
				gTO.setCosto(results.getFloat("Costo"));
				gTO.setStato(Enum.valueOf(Stato.class, results.getString("Stato")));
				gTO.setManager(new Manager(manager));
				
				TOg.add(gTO);
				
			}
			else
				throw new NoEntryException("Non ci sono gite con il Manager indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return TOg;
	}

	@Override
	public void setManager(ManagerTO manager) throws DaoException, NoEntryException {
		Connection con =null;
		String query = "INSERT INTO Manager (Nome, Cognome, Email, Username) VALUES (?, ?, ?, ?);";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, manager.getNome());
			statement.setString(2, manager.getCognome());
			statement.setString(3, manager.getEmail());
			statement.setString(4, manager.getUsername());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public List<ManagerTO> getManager() throws DaoException, NoEntryException {
		List<ManagerTO> ParTO = null;
		Connection con = null;
		String query = "SELECT * from Manager;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				ManagerTO parTO =  new ManagerTO();
				parTO.setUsername(results.getString("username"));
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setEmail(results.getString("Email"));
				
				ParTO.add(parTO);
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return  ParTO;
	}

	@Override
	public List<ManagerTO> getManager(ManagerTO manager) throws DaoException, NoEntryException {
		List<ManagerTO> ParTO = null;
		Connection con = null;
		String query = createQueryString("Select *", manager);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				ManagerTO parTO =  new ManagerTO();
				parTO.setUsername(results.getString("username"));
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setEmail(results.getString("Email"));
				
				ParTO.add(parTO);
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return  ParTO;
	}

	@Override
	public void deleteManager(ManagerTO manager) throws DaoException, NoEntryException {
		Connection con = null;
		String query = createQueryString("DELETE", manager);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateManager(ManagerTO manager) throws DaoException, NoEntryException {
		Connection con = null;
		String query = "UPDATE Manager SET Username = ?, Nome = ?, Cognome = ?, Email=?  WHERE Username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, manager.getUsername());
			statement.setString(2, manager.getNome());
			statement.setString(3, manager.getCognome());
			statement.setString(4, manager.getEmail());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}
	
	private String createQueryString(String beginString, ManagerTO par)
	{
		String query = beginString + " from Manager where ";
		
		if(par.getUsername() != null)
			query += "Manager.sername = '" + par.getUsername() + "' AND ";
		if(par.getNome() != null)
			query += "Manager.Nome = '" + par.getNome() + "' AND ";
		if(par.getCognome() != null)
			query += "Manager.Cognome = '" + par.getCognome() + "' AND ";
		if(par.getEmail() != null)
			query += "Manager.Email = '" + par.getEmail() + "' AND ";
			
		query = query.substring(0, query.length() - 5);
		query += ";";
		
		return query;
	}

	@Override
	public ManagerTO getManager(int id) throws DaoException, NoEntryException {
		ManagerTO manTO = null;
		Connection con = null;
		String query = "SELECT * from Manager where idManager = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, id);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				manTO = new ManagerTO();
				manTO.setNome(results.getString("Nome"));
				manTO.setCognome(results.getString("Cognome"));
				manTO.setEmail(results.getString("Email"));
				manTO.setUsername(results.getString("username"));
				
			}
			else
				throw new NoEntryException("Non ci sono manager con il nome indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return manTO;
	}

}
