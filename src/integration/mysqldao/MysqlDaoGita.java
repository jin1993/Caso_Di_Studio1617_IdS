package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import business.bo.Manager;
import business.bo.Optional;
import business.to.GitaTO;
import business.to.ManagerTO;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import business.to.Sesso;
import business.to.Stato;
import business.to.Tipo;
import integration.mysqldao.MysqlDaoManager;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoGita;

public class MysqlDaoGita implements DaoGita{

	@Override
	public GitaTO getGita(int id) throws DaoException {
		GitaTO gitaTO = null;
		Connection con = null;
		String query = "SELECT * from Gita where idGita = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, id);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				gitaTO = new GitaTO();
				MysqlDaoManager man = new MysqlDaoManager();
				Manager manTO =  new Manager( man.getManager(results.getInt("idManager")));
				gitaTO.setId(results.getInt("idGita"));
				gitaTO.setData(results.getDate("Data").toLocalDate());
				gitaTO.setTipo(Enum.valueOf(Tipo.class, results.getString("Tipo")));
				gitaTO.setMinpart(results.getInt("Nmin"));
				gitaTO.setMaxpart(results.getInt("Nmax"));
				gitaTO.setCosto(results.getFloat("Costo"));
				gitaTO.setStato(Enum.valueOf(Stato.class, results.getString("Stato")));
				gitaTO.setManager(manTO);
				
				List<OptionalTO> ListopTO = getOptionalGita(gitaTO);
			}
			else
				throw new NoEntryException("Non ci sono gite con l'id indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return gitaTO;
	
	}

	@Override
	public List<GitaTO> getGita(LocalDate data) throws DaoException, NoEntryException {
		List<GitaTO> LgitaTO = new LinkedList<GitaTO>();
		Connection con = null;
		String query = "SELECT * from Gita where Data = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setDate(1, java.sql.Date.valueOf(data));
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				GitaTO gitaTO = new GitaTO();
				MysqlDaoManager man = new MysqlDaoManager();
				Manager manTO =  new Manager( man.getManager(results.getInt("idManager")));
				gitaTO.setId(results.getInt("idGita"));
				gitaTO.setData(results.getDate("Data").toLocalDate());
				gitaTO.setTipo(Enum.valueOf(Tipo.class, results.getString("Tipo")));
				gitaTO.setMinpart(results.getInt("Nmin"));
				gitaTO.setMaxpart(results.getInt("Nmax"));
				gitaTO.setCosto(results.getFloat("Costo"));
				gitaTO.setStato(Enum.valueOf(Stato.class, results.getString("Stato")));
				gitaTO.setManager(manTO);
				
				List<OptionalTO> ListopTO = getOptionalGita(gitaTO);
				
				LgitaTO.add(gitaTO);
			}
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LgitaTO;
		}

	public List<GitaTO> getGita(ManagerTO man1) throws DaoException, NoEntryException {
		List<GitaTO> LgitaTO = new LinkedList<GitaTO>();
		Connection con = null;
		String query = "SELECT * from Gita where idManager = ?;";
		PreparedStatement statement = null;
		MysqlDaoManager man = new MysqlDaoManager();
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, man.getIDManager(man1.getUsername()));
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				GitaTO gitaTO = new GitaTO();
				
				Manager manTO =  new Manager( man.getManager(results.getInt("idManager")));
				gitaTO.setId(results.getInt("idGita"));
				gitaTO.setData(results.getDate("Data").toLocalDate());
				gitaTO.setTipo(Enum.valueOf(Tipo.class, results.getString("Tipo")));
				gitaTO.setMinpart(results.getInt("Nmin"));
				gitaTO.setMaxpart(results.getInt("Nmax"));
				gitaTO.setCosto(results.getFloat("Costo"));
				gitaTO.setStato(Enum.valueOf(Stato.class, results.getString("Stato")));
				gitaTO.setManager(manTO);
				
				List<OptionalTO> ListopTO = getOptionalGita(gitaTO);
				
				LgitaTO.add(gitaTO);
			}
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LgitaTO;
		}

	
	@Override
	public GitaTO getGita(GitaTO gita) throws DaoException {
		GitaTO gitaTO = null;
		Connection con = null;
		String query = createQueryString("Select *", gita);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				gitaTO = new GitaTO();
				MysqlDaoManager man = new MysqlDaoManager();
				Manager manTO =  new Manager(man.getManager(results.getInt("idManager")));
				gitaTO.setId(results.getInt("idGita"));
				gitaTO.setData(results.getDate("Data").toLocalDate());
				gitaTO.setTipo(Enum.valueOf(Tipo.class, results.getString("Tipo")));
				gitaTO.setMinpart(results.getInt("Nmin"));
				gitaTO.setMaxpart(results.getInt("Nmax"));
				gitaTO.setCosto(results.getFloat("Costo"));
				gitaTO.setStato(Enum.valueOf(Stato.class, results.getString("Stato")));
				gitaTO.setManager(manTO);
				
				List<OptionalTO> ListopTO = getOptionalGita(gitaTO);
			}
			else
				throw new NoEntryException("Non ci sono gite con l'id indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return gitaTO;
	
	}

	@Override
	public List<GitaTO> getGite() throws DaoException {
		List<GitaTO> LgitaTO = new LinkedList<GitaTO>();
		Connection con = null;
		String query = "SELECT * from Gita;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				GitaTO gitaTO = new GitaTO();
				MysqlDaoManager man = new MysqlDaoManager();
				Manager manTO =  new Manager( man.getManager(results.getInt("idManager")));
				gitaTO.setId(results.getInt("idGita"));
				gitaTO.setData(results.getDate("Data").toLocalDate());
				gitaTO.setTipo(Enum.valueOf(Tipo.class, results.getString("Tipo")));
				gitaTO.setMinpart(results.getInt("Nmin"));
				gitaTO.setMaxpart(results.getInt("Nmax"));
				gitaTO.setCosto(results.getFloat("Costo"));
				gitaTO.setStato(Enum.valueOf(Stato.class, results.getString("Stato")));
				gitaTO.setManager(manTO);
				
				List<OptionalTO> ListopTO = getOptionalGita(gitaTO);
				
				LgitaTO.add(gitaTO);
			}
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LgitaTO;
	}

	@Override
	public void setGita(GitaTO gita) throws DaoException {
		Connection con = null;
		String query = "INSERT INTO Gita (Data, Tipo, Nmin, Nmax, Costo, Stato, idManager) VALUES (?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			MysqlDaoManager man = new MysqlDaoManager();
			statement.setDate(1,java.sql.Date.valueOf(gita.getData()));
			statement.setString(2, gita.getTipo().toString());
			statement.setInt(3, gita.getMinpart());
			statement.setInt(4, gita.getMaxpart());
			statement.setFloat(5, gita.getCosto());
			statement.setString(6, gita.getStato().toString());
			statement.setInt(7, man.getIDManager(gita.getManager().getUsername()));
			
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		
	}

	@Override
	public List<PartecipanteTO> getPartecipantiGita(GitaTO gita) throws DaoException {
		List<PartecipanteTO> LparTO = new LinkedList<PartecipanteTO>();
		Connection con = null;
		String query = "SELECT * from (Prenotazione inner join Partecipante on Partecipante.Username=Prenotazione.Partecipante_usernamePartecipante) where Gita_idGita= ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, gita.getId());
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				PartecipanteTO parTO = new PartecipanteTO();
				parTO.setUsername(results.getString("Username"));
				parTO.setNome(results.getString("Nome"));
				parTO.setCognome(results.getString("Cognome"));
				parTO.setCodf(results.getString("CF"));
				parTO.setSesso(Enum.valueOf(Sesso.class, results.getString("Sesso")));
				parTO.setVia(results.getString("Via"));
				parTO.setNcivico(results.getString("Ncivico"));
				parTO.setCitta(results.getString("Citta"));
				parTO.setNumtessanit(results.getInt("N_tesseras"));
				parTO.setCertificato(results.getString("Certificato"));
				parTO.setDatacert( results.getDate("Data_certificato").toLocalDate());
				parTO.setDatanascita(results.getDate("Anno_nascita").toLocalDate());
				parTO.setEmail(results.getString("Email"));
				
				
				LparTO.add(parTO);
			}
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return LparTO;
	}

	
	
	
	@Override
	public List<OptionalTO> getOptionalGita(GitaTO gita) throws DaoException {
		String query = "Select * from OptionalDisponibiliGita where Gita_idGita = ?;";
		Connection con = null;
		PreparedStatement statement = null;
		List<OptionalTO> listOpt = new LinkedList<OptionalTO>();

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, gita.getId());


			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				int idop = results.getInt("Optional_idOptional");
				OptionalTO optionalTO = new OptionalTO();
				MysqlDaoOptional op = new MysqlDaoOptional();
				optionalTO=op.getOptional(idop);

				listOpt.add(optionalTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;
	}

	@Override
	public void deleteGita(GitaTO gita) throws DaoException {
		Connection con = null;
		String query = createQueryString("DELETE", gita);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		
	}

	@Override
	public void updateGita(GitaTO gita) throws DaoException {
		Connection con = null;
		String query = "UPDATE Gita SET Data = ?, Tipo = ?, Nmin = ?, Nmax = ?, Costo =?, "
				+ "Stato=?, idManager=?  WHERE idGita = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			MysqlDaoManager man = new MysqlDaoManager();
			statement.setDate(1,java.sql.Date.valueOf(gita.getData()));
			statement.setString(2, gita.getTipo().toString());
			statement.setInt(3, gita.getMinpart());
			statement.setInt(4, gita.getMaxpart());
			statement.setFloat(5, gita.getCosto());
			statement.setString(6, gita.getStato().toString());
			statement.setInt(7, man.getIDManager(gita.getManager().getUsername()));
			statement.setInt(8, gita.getId());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}
	
	private String createQueryString(String beginString, GitaTO par)
	{
		String query = beginString + " from Gita where ";
		
		MysqlDaoManager man = new MysqlDaoManager();
		
		if(par.getId() != 0)
			query += "Gita.idGita = '" + par.getId() + "' AND ";
		if(par.getData() != null)
			query += "Gita.Data = '" + par.getData() + "' AND ";
		if(par.getTipo() != null)
			query += "Gita.Tipo = '" + par.getTipo().toString() + "' AND ";
		if(par.getMinpart() != 0)
			query += "Gita.Nmin = '" + par.getMinpart() + "' AND ";
		if(par.getMaxpart() != 0)
			query += "Gita.Nmax = '" + par.getMaxpart() + "' AND ";
		if(par.getCosto() != 0)
			query += "Gita.Costo = '" + par.getCosto() + "' AND ";
		if(par.getStato() != null)
			query += "Gita.Stato = '" + par.getStato().toString() + "' AND ";
		if(par.getManager() != null)
			query += "Gita.idManager = '" + man.getIDManager(par.getManager().getUsername()) + "' AND ";
			
		query = query.substring(0, query.length() - 5);
		query += ";";
		
		return query;
	}

	@Override
	public void setOptionalGita(GitaTO gita, List<OptionalTO> lop) throws DaoException {
	
		List<OptionalTO> Listopgita = getOptionalGita(gita);
		Connection con = null;
		PreparedStatement statement = null;
		
		List<OptionalTO> lop1 = null;
		try {
			con = MysqlDaoFactory.getConnection();
			
			
			if(Listopgita != null) {
				
				lop1 = new LinkedList<OptionalTO>();
				
				for(int i = 0; lop.size()>i; i++) {
					String query = "INSERT INTO OptionalDisponibiliGita VALUES (?, ?);";
					statement = con.prepareStatement(query);
					
					statement.setInt(1, gita.getId());
					statement.setInt(2, lop.get(i).getId());
					
					statement.executeUpdate();
				}
			}
		}catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteOptionalGita(GitaTO gita) throws DaoException {
		String query = "DELETE from OptionalDisponibiliGita where Gita_idGita = ?;";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, gita.getId());

			statement.executeUpdate();

		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

}
