package integration.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import integration.DaoException;
import integration.config.ConfigKeysSpec;
import integration.config.ConfigReader;
import integration.interfaces.*;
import integration.mysqldao.MysqlDaoGita;
import integration.mysqldao.MysqlDaoManager;
import integration.mysqldao.MysqlDaoOptional;
import integration.mysqldao.MysqlDaoPartecipante;
import integration.mysqldao.MysqlDaoPrenotazione;
import integration.mysqldao.MysqlDaoSecData;

public class MysqlDaoFactory extends DaoFactory{

	private static MysqlDaoFactory mysqlDaoFactory;

	private static String host;
	private static String db;
	private static String username;
	private static String password;
	private static String connessione;


	public static MysqlDaoFactory getInstance()
	{
		if (mysqlDaoFactory == null) {
			mysqlDaoFactory = new MysqlDaoFactory();
		}
		return mysqlDaoFactory;
	}

	private MysqlDaoFactory() {
		ConfigReader reader = new ConfigReader(ConfigKeysSpec.getPath());
		host = reader.getProperty(ConfigKeysSpec.getHostkey());
		db = reader.getProperty(ConfigKeysSpec.getDbkey());
		username = reader.getProperty(ConfigKeysSpec.getUserkey());

		/*TODO: per ora verr� usata la password in chiaro, nelle fasi successive
		 * del development consiglierei di memorizzare su config una versione criptata
		 * e decriptarla all'occorrenza
		 */
		password = reader.getProperty(ConfigKeysSpec.getPwdkey());
		connessione = "jdbc:mysql://" + host + "/" + db;
	}

	public static Connection getConnection()throws SQLException
	{

		getInstance();
		Connection con = DriverManager.getConnection(connessione, username, password);

		return con;

	}

	public static void closeConnection(Connection con) throws SQLException
	{
		if(con != null && !con.isClosed())
		{
			con.close();
			con = null;
		}
	}

	@Override
	public DaoAccount getDaoAccount() throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public DaoGita getDaoGita() throws DaoException {
		// TODO Auto-generated method stub
		return (DaoGita) createDao(MysqlDaoGita.class);
	}

	@Override
	public DaoManager getDaoManager() throws DaoException {
		// TODO Auto-generated method stub
		return (DaoManager) createDao(MysqlDaoManager.class);
	}

	@Override
	public DaoPartecipante getDaoPartecipante() throws DaoException {
		// TODO Auto-generated method stub
		return (DaoPartecipante) createDao(MysqlDaoPartecipante.class);
	}

	@Override
	public DaoPrenotazione getDaoPrenotazione() throws DaoException {
		// TODO Auto-generated method stub
		return (DaoPrenotazione) createDao(MysqlDaoPrenotazione.class);
	}

	@Override
	public DaoOptional getDaoOptional() throws DaoException {
		// TODO Auto-generated method stub
		return (DaoOptional) createDao(MysqlDaoOptional.class);
	}

	@Override
	public DaoSecData getDaoSecData() throws DaoException {
		return (DaoSecData) createDao(MysqlDaoSecData.class);
	}
}
