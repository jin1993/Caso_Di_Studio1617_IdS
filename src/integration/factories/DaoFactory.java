package integration.factories;

import integration.DaoException;
import integration.interfaces.*;

public abstract class DaoFactory {

	public static final int MYSQL = 1;

	public static DaoFactory getDaoFactory(int choice)
	{
		DaoFactory dao = null;

		switch(choice)
		{

			case MYSQL:
				dao = MysqlDaoFactory.getInstance();
				break;
			default:
				dao = null;
				break;
		}

		return dao;
	}

	@SuppressWarnings("rawtypes")
	protected static Object createDao(Class c) throws DaoException
	{
		try {
			return c.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new DaoException(e);
		}
	}

	public abstract DaoAccount getDaoAccount() throws DaoException;
	public abstract DaoGita getDaoGita() throws DaoException;
	public abstract DaoManager getDaoManager() throws DaoException;
	public abstract DaoOptional getDaoOptional() throws DaoException;
	public abstract DaoPartecipante getDaoPartecipante() throws DaoException;
	public abstract DaoPrenotazione getDaoPrenotazione() throws DaoException;
	public abstract DaoSecData getDaoSecData() throws DaoException;

}
