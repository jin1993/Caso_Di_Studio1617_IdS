package integration.config;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Properties;

import integration.DaoConfigurationException;

public class ConfigWriter {

	private Properties properties;
	private OutputStream output = null;
	
	public ConfigWriter(String path) {
		
		try
		{
			output = new FileOutputStream(path);
			properties = new Properties();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void setProperty(String pName, String pValue)
	{
		properties.setProperty(pName, pValue);
	}
	
	public void store(OutputStream out, String comments)
	{
		try {
			properties.store(out, comments);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void store(Writer wrt, String comments)
	{
		try {
			properties.store(wrt, comments);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void close()
	{
		if(output != null)
		{
			try {
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
