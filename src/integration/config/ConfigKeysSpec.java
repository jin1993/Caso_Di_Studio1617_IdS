package integration.config;

public class ConfigKeysSpec {

	private final static String PATH = "src/integration/config/mysql.properties";
	private final static String HOSTKEY = "host";
	private final static String DBKEY = "database";
	private final static String USERKEY = "dbuser";
	private final static String PWDKEY = "dbpassword";


	public static String getPath() {
		return PATH;
	}
	public static String getHostkey() {
		return HOSTKEY;
	}
	public static String getDbkey() {
		return DBKEY;
	}
	public static String getUserkey() {
		return USERKEY;
	}
	public static String getPwdkey() {
		return PWDKEY;
	}
	
}
