package integration.config;

import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.io.FileInputStream;
import java.io.IOException;

public class ConfigReader {

	private Properties properties;
	private InputStream input = null;
	
	public ConfigReader(String path) {
		try
		{
			input = new FileInputStream(path);
			properties = new Properties();
			properties.load(input);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public String getProperty(String propName)
	{
		if(properties.containsKey(propName))
			return properties.getProperty(propName);
		else
			return null;
	}
	
	public Set<Object> keySet()
	{
		return properties.keySet();
	}
	
	public void close()
	{
		if(input != null)
		{
			try
			{
				input.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}

}
