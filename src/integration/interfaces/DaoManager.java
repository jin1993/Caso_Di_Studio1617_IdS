package integration.interfaces;

import java.util.List;

import business.to.GitaTO;
import business.to.ManagerTO;
import integration.DaoException;
import integration.NoEntryException;

public interface DaoManager {

	public ManagerTO getManager(String username) throws DaoException, NoEntryException;
	public ManagerTO getManager(int id) throws DaoException, NoEntryException;
	public int getIDManager(String username)throws DaoException, NoEntryException;
	
	public List<GitaTO> getGite(ManagerTO manager)throws DaoException, NoEntryException;
	
	public void setManager(ManagerTO manager)throws DaoException, NoEntryException;
	
	public List<ManagerTO> getManager()throws DaoException, NoEntryException;
	public List<ManagerTO> getManager(ManagerTO manager)throws DaoException, NoEntryException;
	public void deleteManager(ManagerTO manager)throws DaoException, NoEntryException;
	public void updateManager(ManagerTO manager)throws DaoException, NoEntryException;
}
