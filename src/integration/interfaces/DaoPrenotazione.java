package integration.interfaces;

import java.time.LocalDate;
import java.util.List;

import business.to.GitaTO;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import business.to.PrenotazioneTO;

public interface DaoPrenotazione {

	
	//se serve implementare inerfaccia e metodo che restituisca una sola prenotazione
	public List<PrenotazioneTO> getPrenotazioni(PartecipanteTO partecipante);
	public List<PrenotazioneTO> getPrenotazioni(LocalDate data);
	public PrenotazioneTO getPrenotazione(PartecipanteTO partecipante, GitaTO gita);
	public List<PrenotazioneTO> getPrenotazioni();
	public List<PartecipanteTO> getPrenotazioni(GitaTO gita);
	public List<OptionalTO> getOptionalPrenotazione(PrenotazioneTO prenotazione);
	
	public void setPrenotazione(PrenotazioneTO prenotazione);
	public void setOptionalPrenotazione(PrenotazioneTO prenotazione, List<OptionalTO> optional);
	public void deletePrenotazione(PrenotazioneTO prenotazione);
	public void deleteOptionalPrenotazione(PrenotazioneTO prenotazione);
	public void updatePrenotazione(PrenotazioneTO prenotazione);
	public void updateOptionalPrenotazione(PrenotazioneTO prenotazione, List<OptionalTO> optional);
}
