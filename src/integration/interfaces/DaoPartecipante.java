package integration.interfaces;

import java.util.List;

import business.to.PartecipanteTO;

public interface DaoPartecipante {
	
	public PartecipanteTO getPartecipanteByEmail(String email) ;
	public PartecipanteTO getPartecipante(String username);
	public List<PartecipanteTO> getPartecipante();
	public List<PartecipanteTO> getPartecipante(PartecipanteTO partecipante);
	
	public void setPartecipante(PartecipanteTO partecipante);
	public void deletePartecipante(PartecipanteTO partecipante);
	public void updatePartecipante(PartecipanteTO partecipante);
	
}
