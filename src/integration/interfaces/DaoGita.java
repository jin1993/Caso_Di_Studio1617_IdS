package integration.interfaces;

import java.time.LocalDate;
import java.util.List;

import business.to.GitaTO;
import business.to.ManagerTO;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import integration.DaoException;
import integration.NoEntryException;

public interface DaoGita {
	
	public GitaTO getGita(int id) throws DaoException;
	public List<GitaTO> getGita(LocalDate data)  throws DaoException, NoEntryException;
	public List<GitaTO> getGita(ManagerTO man)  throws DaoException, NoEntryException;
	
	public GitaTO getGita(GitaTO gita)  throws DaoException;
	public List<GitaTO> getGite()  throws DaoException;
	
	public void setGita(GitaTO gita) throws DaoException;
	public List<PartecipanteTO> getPartecipantiGita(GitaTO gita) throws DaoException;
	public List<OptionalTO> getOptionalGita(GitaTO gita)throws DaoException;

	public void deleteGita(GitaTO gita)throws DaoException;
	public void updateGita(GitaTO gita)throws DaoException;
	public void setOptionalGita(GitaTO gita, List<OptionalTO> lop) throws DaoException;
	public void deleteOptionalGita(GitaTO gita) throws DaoException;
}
