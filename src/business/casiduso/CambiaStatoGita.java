package business.casiduso;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import business.OperazioneNonCompletata;
import business.bo.Gita;
import business.bo.Partecipante;
import business.to.GitaTO;
import business.to.PartecipanteTO;
import business.to.Stato;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoPrenotazione;

public class CambiaStatoGita implements AppServiceInt{

	private int idGita;
	private String stato;
	
	@Override
	public Object execute() throws OperazioneNonCompletata, MessagingException {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		int max, min, prenotazioni;
		boolean validity = false;
		NotificaMail inviomail = new NotificaMail();
		DaoPrenotazione daop = null;
		DaoGita dg = null;
		List<String> email =null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

			try {
				
				System.out.println(stato);
				daop = daoFactory.getDaoPrenotazione();
				dg = daoFactory.getDaoGita();
				GitaTO gita =dg.getGita(idGita);
				if(daop.getPrenotazioni(dg.getGita(idGita))==null)
					prenotazioni = 0;
				else
					prenotazioni=daop.getPrenotazioni(dg.getGita(idGita)).size();
				max = dg.getGita(idGita).getMaxpart();
				min = dg.getGita(idGita).getMinpart();
				LocalDate data1 = dg.getGita(idGita).getData().minusDays(2);
				LocalDate data2 = dg.getGita(idGita).getData().plusDays(1);
				if(LocalDate.now().equals(dg.getGita(idGita).getData()) || stato.equals("INCORSO"))
					gita.setStato(Stato.INCORSO);
				if(LocalDate.now().equals(data1)  || stato.equals("INCORSO"))
					gita.setStato(Stato.INCORSO);
				if(LocalDate.now().equals(data2)  || stato.equals("TERMINATA"))
					gita.setStato(Stato.TERMINATA);
				if((LocalDate.now().equals(data1) && prenotazioni<min)  || stato.equals("ANNULLATA")) {
					gita.setStato(Stato.ANNULLATA);
					//manda mail partecipanti
					if(prenotazioni!=0){
					List<PartecipanteTO> lpar = daop.getPrenotazioni(dg.getGita(idGita));
					for(int i = 0 ; lpar.size()>i; i++) {
						email.add(lpar.get(i).getEmail());
					}
					String oggetto = "Annullamento gita";
					String msg = "La gita a cui si � prenotato � stata annullata.";
					inviomail.inviaMail(email, oggetto, msg, dg.getGita(idGita).getManager().getEmail());
					}
				}
				if(prenotazioni>max  || stato.equals("SOLDOUT"))
					gita.setStato(Stato.SOLDOUT);
				
				dg.updateGita(gita);
			}	
			catch(DaoException e)
			{
				MessageDisplayer.showErrorMessage(e.toString());
			}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.idGita = (int) parameters.get(0);
		this.stato = (String) parameters.get(1);
	}

}
