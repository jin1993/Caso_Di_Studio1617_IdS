package business.casiduso;

import business.OperazioneNonCompletata;
import business.casiduso.PasswordStorage.CannotPerformOperationException;
import business.casiduso.PasswordStorage.InvalidHashException;

public class CryptoService implements CryptoInt {
	
	public CryptoService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String encrypt(String pwd) throws OperazioneNonCompletata {
		try {
			return PasswordStorage.createHash(pwd);
		} catch (CannotPerformOperationException e) {
			throw new OperazioneNonCompletata(e);
		}
	}

	@Override
	public boolean verify(String pwd, String enc) throws OperazioneNonCompletata {
		
		try {
			return PasswordStorage.verifyPassword(pwd,  enc);
		} catch (CannotPerformOperationException | InvalidHashException e) {
			throw new OperazioneNonCompletata(e);
		}
	}

}
