package business.casiduso;

import business.OperazioneNonCompletata;

public interface CryptoInt {
	public String encrypt(String pwd) throws OperazioneNonCompletata;
	public boolean verify(String pwd, String enc) throws OperazioneNonCompletata;
}
