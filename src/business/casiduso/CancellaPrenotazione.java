package business.casiduso;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Gita;
import business.bo.Optional;
import business.bo.Partecipante;
import business.bo.Prenotazione;
import business.to.OptionalTO;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;

public class CancellaPrenotazione implements AppServiceInt {
	private int idGita;
	private String usernameP;
	
	
	@Override
	public Object execute() throws OperazioneNonCompletata {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		DaoPrenotazione daop = null;
		DaoPartecipante daopar = null;
		DaoGita dg = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		try {
			daop = daoFactory.getDaoPrenotazione();
			daopar = daoFactory.getDaoPartecipante();
			dg = daoFactory.getDaoGita();
			
			Prenotazione p = new Prenotazione(daop.getPrenotazione(daopar.getPartecipante(usernameP), dg.getGita(idGita)));
			
			daop.deleteOptionalPrenotazione(p);
			daop.deletePrenotazione(p);
			
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		
		
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.idGita = (int) parameters.get(0);
		this.usernameP = (String) parameters.get(1);
	}

}
