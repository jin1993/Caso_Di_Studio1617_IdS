package business.casiduso;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Manager;
import business.bo.Partecipante;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoManager;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoSecData;
import presentation.Logger;

public class EffettuaLogin implements AppServiceInt {

	private String username;
	private String password;

	@Override
	public Object execute() {

		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		Logger logger = Logger.getInstance();
		UserRole backupRole = logger.getUserRole();
		String backupUsername = logger.getUsername();
		//CryptoInt cryptoApp = new CryptoService();
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			DaoPartecipante dp = daoFactory.getDaoPartecipante();
			DaoManager dm = daoFactory.getDaoManager();
			DaoSecData daoSecData = daoFactory.getDaoSecData();
			
			CryptoInt cryptoApp = serviceFactory.getEncryptionService("Encryption");

			Partecipante par = new Partecipante();
			par.setUsername(username);
			Manager man = new Manager();
			man.setUsername(username);


			String dbPwd = daoSecData.getUserHash(username);

			if(cryptoApp.verify(password, dbPwd))
			{
				if(par.isPartecipanteInStorageByUsername())
				{
					logger.setUserRole(UserRole.PARTECIPANTE);
					logger.setUsername(username);
				}
				else if(man.isManagerInStorageByUsername())
				{
					logger.setUsername(username);
					logger.setUserRole(UserRole.MANAGER);
					man = new Manager(dm.getManager(username));
				}
				else {
					
						logger.setUserRole(backupRole);
						logger.setUsername(backupUsername);

					}
			}
			else
			{
				throw new NoEntryException();
			}

		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			logger.setUserRole(backupRole);
			logger.setUsername(backupUsername);

			MessageDisplayer.showErrorMessage(e.toString());
		}
		catch(NoEntryException e)
		{
			logger.setUserRole(backupRole);
			logger.setUsername(backupUsername);

			NoEntryException noEntry = new NoEntryException("Username o password errate");

			MessageDisplayer.showErrorMessage(e.toString());
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {

		username = (String) parameters.get(0);
		password = (String) parameters.get(1);

	}


}
