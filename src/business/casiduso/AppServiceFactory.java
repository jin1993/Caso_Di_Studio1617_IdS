package business.casiduso;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import business.OperazioneNonCompletata;

public class AppServiceFactory {
	
	private final static String PATH = "src/integration/config/servicemapping.xml";
	private static Map<String, ServiceProperties> serviceMap;
	
	private static AppServiceFactory factory;
	
	public static AppServiceFactory getInstance()
	{
		if(factory == null)
		{
			factory = new AppServiceFactory();
		}
		
		return factory;
	}

	private AppServiceFactory() {
		
		try {
			loadMap();
		} catch (OperazioneNonCompletata e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void loadMap() throws OperazioneNonCompletata
	{
		File serviceFile = new File(PATH);
		try
		{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(serviceFile);
			
			NodeList nList = doc.getElementsByTagName("service");
			
			serviceMap = new HashMap<String, ServiceProperties>();
			
			for(int i = 0; i < nList.getLength(); i++)
			{
				Node node = nList.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{	
					Element element = (Element) node;
					
					ServiceProperties sp = new ServiceProperties();
					sp.setServiceImpl(Class.forName((element.getElementsByTagName("class").item(0)).getTextContent()));
					
					Node child;
					child = element.getElementsByTagName("view").item(0);
					
					Node c;
					//System.out.println(child.getTextContent());
					
					child = element.getElementsByTagName("return").item(0);
					
					if(!child.getTextContent().isEmpty())
					{
						sp.setReturnClass(Class.forName(child.getTextContent()));
					}
					
					serviceMap.put(element.getAttribute("name"), sp);
				}
			}
			
		}
		catch(ParserConfigurationException | SAXException | IOException | ClassNotFoundException | DOMException e)
		{
			throw new OperazioneNonCompletata(e);
		}
//		if(serviceMap == null)
//		{
//			serviceMap = new HashMap<String, Class>();
//			ConfigReader cfReader = new ConfigReader(PATH);
//			
//			Set<Object> keyset = cfReader.keySet();
//			
//			for(Object o : keyset)
//			{
//				String key = (String) o;
//				
//				try {
//					serviceMap.put(key, Class.forName(cfReader.getProperty(key)));
//				} catch (ClassNotFoundException e) {
//					// TODO Auto-generated catch block
//					throw new OperazioneNonCompletata(e);
//				}
//			}
//			
//			cfReader.close();
//		}
		
	}
	
	public AppServiceInt getService(String name) throws OperazioneNonCompletata
	{	
		try {
			return (AppServiceInt) serviceMap.get(name).getServiceImpl().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			throw new OperazioneNonCompletata(e);
		}
	}
	
	
	public CryptoInt getEncryptionService(String name) throws OperazioneNonCompletata
	{
		try {
			return (CryptoInt) serviceMap.get(name).getServiceImpl().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			throw new OperazioneNonCompletata(e);
		}
	}
	

}
