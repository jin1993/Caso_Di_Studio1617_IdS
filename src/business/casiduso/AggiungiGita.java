package business.casiduso;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Gita;
import business.bo.Manager;
import business.bo.Optional;
import business.to.Stato;
import business.to.Tipo;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoManager;
import integration.interfaces.DaoOptional;

public class AggiungiGita implements AppServiceInt{
	private int id;
	private LocalDate data;
	private String tipo;
	private int minpart;
	private int maxpart;
	private float costo;
	private String stato;
	private int manager;
	
	protected List<String> optional;

	@Override
	public Object execute() throws OperazioneNonCompletata {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoGita daoGita = null;
		DaoManager daoManager = null;
		DaoOptional daoOptional = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		try {
			daoGita = daoFactory.getDaoGita();
			daoManager = daoFactory.getDaoManager();
			daoOptional = daoFactory.getDaoOptional();
			Tipo t = null;
			Stato s = null;
			
			if(tipo.equals("MONGOLFIERA"))
				t = Tipo.MONGOLFIERA;
			if(tipo.equals("MONTAGNA"))
				t= Tipo.MONTAGNA;
			
			switch (stato) {
			case "INCORSO": 
				s = Stato.INCORSO;
				break;
			case "TERMINATA" :
				s= Stato.TERMINATA;
				break;
			case "ANNULLATA" :
				s = Stato.ANNULLATA;
				break;
			case "SOLDOUT":
				s = Stato.SOLDOUT;
				break;
			case "DASVOLGERE" : 
				s = Stato.DASVOLGERE;
				break;
			default:
				break;
			}
			
			
			List<Optional> op = new LinkedList<>();
			for(int i =0; optional.size()>i; i++) {
				Optional opt = new Optional(daoOptional.getOptional(optional.get(i)));
				op.add(opt);
			}
			Gita g = new Gita(id, data, t, minpart, maxpart, costo, s, new Manager(daoManager.getManager(manager)), op);
			
			if(!g.isGitaInStorageById())
				daoGita.setGita(g);
			else
				throw new OperazioneNonCompletata();
			
		}catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.id= (int) parameters.get(0);
		this.data=(LocalDate)parameters.get(1);
		this.tipo= (String)parameters.get(2);
		this.minpart=(int)parameters.get(3);
		this.maxpart=(int)parameters.get(4);
		this.costo=(float)parameters.get(5);
		this.stato=(String)parameters.get(6);
		this.manager=(int)parameters.get(7);
		this.optional=(List<String>)parameters.get(8);
	}

}
