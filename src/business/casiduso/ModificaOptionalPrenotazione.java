package business.casiduso;

import java.time.LocalDate;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Optional;
import business.to.OptionalTO;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;

public class ModificaOptionalPrenotazione implements AppServiceInt {
	private int IdGita;
	private String usernameP;
	private List<String> nomeopt;


	@Override
	public Object execute() throws OperazioneNonCompletata {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoO = null;
		DaoGita daog = null;
		DaoPrenotazione dp = null;
		DaoPartecipante dpar = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		try {
			daoO = daoFactory.getDaoOptional();
			daog = daoFactory.getDaoGita();
			dp = daoFactory.getDaoPrenotazione();
			dpar =  daoFactory.getDaoPartecipante();
			List<OptionalTO> lop = null;
			
			for(int i=0; nomeopt.size()>i; i++) {
				Optional op = new Optional(daoO.getOptional(nomeopt.get(i)));
				lop.add(op);
			}
			
			dp.updateOptionalPrenotazione(dp.getPrenotazione(dpar.getPartecipante(usernameP), daog.getGita(IdGita)), lop);
			
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {

		this.IdGita = (int) parameters.get(0);
		this.usernameP = (String) parameters.get(1);
		this.nomeopt = (List<String>) parameters.get(2);

	}

}
