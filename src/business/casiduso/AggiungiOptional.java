package business.casiduso;

import java.util.List;
import business.bo.Optional;

import business.OperazioneNonCompletata;
import business.bo.Manager;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoManager;
import integration.interfaces.DaoOptional;

public class AggiungiOptional implements AppServiceInt {
	private int id;
	private String nome;
	private float costo;
	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoO = null;
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		
		try {
			daoO = daoFactory.getDaoOptional();
			Optional op = new Optional(id, nome, costo);
		
			if(!op.isOptionalInStorage())
				daoO.setOptional(op.getData());
			else
				throw new OperazioneNonCompletata();
		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		return null;
	}
	@Override
	public void init(List<Object> parameters) {
		this.id=(int) parameters.get(0);
		this.nome=(String) parameters.get(1);	
		this.costo=(float) parameters.get(2);
	}
	
	
}
