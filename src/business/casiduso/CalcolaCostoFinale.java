package business.casiduso;

import java.util.List;

import javax.mail.MessagingException;

import business.OperazioneNonCompletata;
import business.bo.Optional;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import business.to.Stato;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;

public class CalcolaCostoFinale implements AppServiceInt {
	private String usernameP;
	private int idGita;
	private List<Integer> opt;
	
	
	
	@Override
	public Object execute() throws OperazioneNonCompletata, MessagingException {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		float somma = 0;
		DaoPrenotazione daop = null;
		DaoGita dg = null;
		DaoPartecipante dp = null;
		DaoOptional dop = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

			try {
				daop = daoFactory.getDaoPrenotazione();
				dg = daoFactory.getDaoGita();
				dp = daoFactory.getDaoPartecipante();
				dop = daoFactory.getDaoOptional();
				
				somma = somma + dg.getGita(idGita).getCosto();
				
				//List<OptionalTO> lop = daop.getOptionalPrenotazione(daop.getPrenotazione(dp.getPartecipante(usernameP), dg.getGita(idGita)));
				
				for(int i = 0; opt.size()>i; i++)
					somma =somma + dop.getOptional(opt.get(i)).getCosto();
				
			}	
			catch(DaoException e)
			{
				MessageDisplayer.showErrorMessage(e.toString());
			}

			
		return somma;
	}

	@Override
	public void init(List<Object> parameters) {
		this.idGita = (int) parameters.get(0);
		this.usernameP = (String) parameters.get(1);
		this.opt = (List<Integer>) parameters.get(2);
	}

}
