package business.casiduso;

import java.time.LocalDate;
import java.util.List;

import javax.mail.MessagingException;

import business.OperazioneNonCompletata;
import business.to.PartecipanteTO;
import business.to.Stato;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoPrenotazione;

public class GestisciCodaPrenotazioni implements AppServiceInt{
	private int idGita;
	private int newmax;

	@Override
	public Object execute() throws OperazioneNonCompletata, MessagingException {
DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		int max, min, prenotazioni;
		NotificaMail inviomail = new NotificaMail();
		DaoPrenotazione daop = null;
		DaoGita dg = null;
		List<String> email =null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

			try {
				daop = daoFactory.getDaoPrenotazione();
				dg = daoFactory.getDaoGita();
				prenotazioni=daop.getPrenotazioni(dg.getGita(idGita)).size();
				max = dg.getGita(idGita).getMaxpart();
				min = dg.getGita(idGita).getMinpart();
				
				if(newmax>max && prenotazioni>max) {
					List<PartecipanteTO> lpar = daop.getPrenotazioni(dg.getGita(idGita));
					for(int i =max; newmax>i; i++) {
						email.add(lpar.get(i).getEmail());
					}
					String oggetto = "Partecipazione alla gita";
					String msg = "La gita a cui si � prenotato ha aumentato il numero max di partecipanti e lei � incluso in essa.";
					inviomail.inviaMail(email, oggetto, msg, dg.getGita(idGita).getManager().getEmail());
					dg.getGita(idGita).setStato(Stato.SOLDOUT);
					dg.getGita(idGita).setMaxpart(newmax);
				}
			}	
			catch(DaoException e)
			{
				MessageDisplayer.showErrorMessage(e.toString());
			}

			
		return null;
	}

	@Override
	public void init(List<Object> parameters) {

		this.idGita = (int) parameters.get(0);
		this.newmax = (int) parameters.get(1);		
	}
	

}
