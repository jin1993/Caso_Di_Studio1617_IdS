package business.casiduso;

import java.util.List;

import javax.mail.MessagingException;

import business.OperazioneNonCompletata;
import business.bo.Optional;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPrenotazione;

public class ModificaOptionalGita implements AppServiceInt {

	private int IdGita;
	private List<String> nomeOp;

	@Override
	public Object execute() throws MessagingException {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoO = null;
		DaoGita daog = null;
		DaoPrenotazione daop = null;
		List<String> email = null;
		NotificaMail inviomail = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		try {
			daoO = daoFactory.getDaoOptional();
			daog = daoFactory.getDaoGita();
			daop = daoFactory.getDaoPrenotazione();
			List<OptionalTO> lop = null;
			
			for(int i=0; nomeOp.size()>i; i++) {
				Optional op = new Optional(daoO.getOptional(nomeOp.get(i)));
				lop.add(op);
			}
			
			daog.deleteOptionalGita(daog.getGita(IdGita));
			daog.setOptionalGita(daog.getGita(IdGita), lop);
			
			
			List<PartecipanteTO> lpar = daop.getPrenotazioni(daog.getGita(IdGita));
			for(int i = 0 ; lpar.size()>i; i++) {
				email.add(lpar.get(i).getEmail());
			}
			String oggetto = "Cambiamento optional gita";
			String msg = "La gita a cui si � prenotato � stata modificata.";
			inviomail.inviaMail(email, oggetto, msg, daog.getGita(IdGita).getManager().getEmail());
			
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		return null;
	}
	@Override
	public void init(List<Object> parameters) {
		this.IdGita=(int) parameters.get(0);
		this.nomeOp=(List<String>) parameters.get(1);
	}

}
