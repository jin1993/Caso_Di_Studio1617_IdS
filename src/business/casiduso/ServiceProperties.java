package business.casiduso;

public class ServiceProperties {
	
	private Class serviceImpl;
	private Class returnClass;

	public Class getServiceImpl() {
		return serviceImpl;
	}

	public void setServiceImpl(Class serviceImpl) {
		this.serviceImpl = serviceImpl;
	}


	public Class getReturnClass() {
		return returnClass;
	}

	public void setReturnClass(Class returnClass) {
		this.returnClass = returnClass;
	}

	public ServiceProperties(Class serviceImpl, Class returnClass) {
		this.serviceImpl = serviceImpl;
		this.returnClass = returnClass;
	}
	
	public ServiceProperties() {}

}
