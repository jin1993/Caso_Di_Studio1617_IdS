package business.casiduso;

import java.util.List;

import javax.mail.MessagingException;

import business.OperazioneNonCompletata;

public interface AppServiceInt {
	public Object execute() throws OperazioneNonCompletata, MessagingException;
	public void init(List<Object> parameters);
}
