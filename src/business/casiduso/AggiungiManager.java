package business.casiduso;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Manager;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoManager;

public class AggiungiManager implements AppServiceInt {

	private String username;
	private String nome;
	private String cognome;
	private String email;
	
	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoManager daoM = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		
		try {
			daoM = daoFactory.getDaoManager();
			Manager man = new Manager(username, nome, cognome, email);
		
			if(!man.isManagerInStorageByUsername())
				daoM.setManager(man.getData());
			else
				throw new OperazioneNonCompletata();
		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.username=(String) parameters.get(1);
		this.nome=(String) parameters.get(2);
		this.cognome=(String) parameters.get(0);
		this.email=(String) parameters.get(3);
	}
	
}
