package business.casiduso;

import java.util.List;

import business.OperazioneNonCompletata;
import presentation.Logger;

public class EffettuaLogout implements AppServiceInt {

	@Override
	public Object execute() {
		
		Logger logger = Logger.getInstance();
		logger.setUsername("ANOMIMO");
		logger.setUserRole(UserRole.ANONIMO);
		
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}

}
