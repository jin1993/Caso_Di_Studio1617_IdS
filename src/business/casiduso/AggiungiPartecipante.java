package business.casiduso;

import java.time.LocalDate;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Manager;
import business.bo.Optional;
import business.bo.Partecipante;
import business.to.OptionalTO;
import business.to.Sesso;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;

public class AggiungiPartecipante implements AppServiceInt {
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private String codf;
	//private enum Sesso {MALE, FEMALE};
	private String sesso;
	private String via;
	private String ncivico;
	private String citta;
	private LocalDate datanascita;
	private int numtessanit;
	private String Certificato;
	private LocalDate datacert;
	

	@Override
	public Object execute() throws OperazioneNonCompletata {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		DaoPartecipante daop = null;

		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

			try {
				daop = daoFactory.getDaoPartecipante();
				Sesso s = null;
				if(sesso.equals("MALE")) {
					 s = Sesso.M;
				}
				else {
					 s = Sesso.F;
				}
				
				Partecipante man = new Partecipante(username, email, nome, cognome, codf, s , via, ncivico, citta, datanascita, datacert, numtessanit);
			
				if(!man.isPartecipanteInStorageByUsername())
					daop.setPartecipante(man.getData());
				else
					throw new OperazioneNonCompletata();
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.username = (String) parameters.get(0);
		this.email = (String) parameters.get(1);
		this.nome = (String) parameters.get(2);
		this.cognome = (String) parameters.get(3);
		this.codf=(String) parameters.get(4);
		this.citta=(String) parameters.get(5);
		this.datacert=(LocalDate) parameters.get(6);
		this.datanascita=(LocalDate) parameters.get(7);
		this.ncivico=(String) parameters.get(8);
		this.numtessanit=(int) parameters.get(9);
		this.sesso=(String) parameters.get(10);
		this.via=(String) parameters.get(11);
	}

}
