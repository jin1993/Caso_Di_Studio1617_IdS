package business.casiduso;

import java.time.LocalDate;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Partecipante;
import business.to.Sesso;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoSecData;

public class Registrazione  implements AppServiceInt{
	private Partecipante par;
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private String codf;
	private Sesso sesso;
	private String via;
	private String ncivico;
	private String citta;
	private LocalDate datanascita;
	private int numtessanit;
	private String Certificato;
	private LocalDate datacert;
	private String pws;

	public Registrazione(){}

	public String getPws() {
		return pws;
	}

	public void setPws(String pws) {
		this.pws = pws;
	}

	public Registrazione(Partecipante par,String pws ){
		this.pws = pws;
		this.par = par;
	}

	/**/
	@Override
	public Object execute(){
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoPartecipante daopar = null;
		DaoSecData daoSecData = null;
		

		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		try
		{
			daopar = daoFactory.getDaoPartecipante();
			daoSecData = daoFactory.getDaoSecData();

			if(par.isPartecipanteInStorageByEmail())
				throw new OperazioneNonCompletata("Username gi� esistente");
			else if(par.isPartecipanteInStorageByUsername())
				throw new OperazioneNonCompletata("Email gi� esistente fra le registrazioni");
			else
			{
				CryptoInt cryptoService = serviceFactory.getEncryptionService("Encryption");
				String hashedPwd = cryptoService.encrypt(pws);
				daoSecData.setUserHash(par.getUsername(), hashedPwd);

				daopar.setPartecipante(par.getData());

			}

		} catch (DaoException | OperazioneNonCompletata e) {
			MessageDisplayer.showErrorMessage(e.toString());
		}
		return null;
	}


	@Override
	public void init(List<Object> parameters) {
		this.username =(String) parameters.get(0);
		this.email = (String) parameters.get(1);
		this.nome = (String) parameters.get(2);
		this.cognome = (String) parameters.get(3);
		this.codf=(String) parameters.get(4);
		this.citta=(String) parameters.get(5);
		this.datacert=(LocalDate) parameters.get(6);
		this.datanascita=(LocalDate) parameters.get(7);
		this.ncivico=(String) parameters.get(8);
		this.numtessanit=(int) parameters.get(9);
		this.sesso=(Sesso) parameters.get(10);
		this.via=(String) parameters.get(11);
		this.pws=(String) parameters.get(12);
		
		par = new Partecipante(username, email, nome, cognome, codf, sesso, via, citta, ncivico, datanascita, datacert, numtessanit);
	}

	;
}
