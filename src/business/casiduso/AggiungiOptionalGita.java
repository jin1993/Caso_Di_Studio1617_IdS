package business.casiduso;

import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Optional;
import business.to.OptionalTO;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;

public class AggiungiOptionalGita implements AppServiceInt{
	
	private int IdGita;
	private List<String> nomeOp;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoO = null;
		DaoGita daog = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();
		
		try {
			daoO = daoFactory.getDaoOptional();
			daog = daoFactory.getDaoGita();
			List<OptionalTO> lop = new LinkedList<>();
			
			for(int i=0; nomeOp.size()>i; i++) {
				Optional op = new Optional(daoO.getOptional(nomeOp.get(i)));
				lop.add(op);
			}
			
			daog.setOptionalGita(daog.getGita(IdGita), lop);
			
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		return null;
	}
	@Override
	public void init(List<Object> parameters) {
		this.IdGita=(int) parameters.get(0);
		this.nomeOp=(List<String>) parameters.get(1);
	}
	
	

}
