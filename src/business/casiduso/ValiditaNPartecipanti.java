package business.casiduso;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Gita;
import business.bo.Optional;
import business.bo.Partecipante;
import business.bo.Prenotazione;
import business.to.OptionalTO;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;

public class ValiditaNPartecipanti implements AppServiceInt {

	private int idGita;
	
	@Override
	public Object execute() throws OperazioneNonCompletata {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		int max, min, prenotazioni;
		boolean validity = false;
		DaoPrenotazione daop = null;
		DaoGita dg = null;
		
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

			try {
				daop = daoFactory.getDaoPrenotazione();
				dg = daoFactory.getDaoGita();
				prenotazioni=daop.getPrenotazioni(dg.getGita(idGita)).size();
				max = dg.getGita(idGita).getMaxpart();
				min = dg.getGita(idGita).getMinpart();
				
				if(prenotazioni<min)
					validity = false;
				else 
					validity = true;
				
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		
		
		return validity;
	}

	@Override
	public void init(List<Object> parameters) {
		this.idGita = (int) parameters.get(0);
	}

}
