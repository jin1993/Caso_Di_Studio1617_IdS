package business.casiduso;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Gita;
import business.bo.Optional;
import business.bo.Partecipante;
import business.bo.Prenotazione;
import business.to.OptionalTO;
import business.to.Sesso;
import errorhandling.MessageDisplayer;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoPartecipante;
import integration.interfaces.DaoPrenotazione;

public class AggiungiPrenotazione implements AppServiceInt {
	private int IdGita;
	private String usernameP;
	private LocalDate datapren;
	private List<Integer> nomeopt ;

	@Override
	public Object execute() throws OperazioneNonCompletata {
		
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		
		DaoOptional dop = null;
		DaoPrenotazione daop = null;
		DaoPartecipante daopar = null;
		DaoGita dg = null;
		

		AppServiceFactory serviceFactory =  AppServiceFactory.getInstance();

			try {
				daop = daoFactory.getDaoPrenotazione();
				daopar = daoFactory.getDaoPartecipante();
				dg = daoFactory.getDaoGita();
				dop = daoFactory.getDaoOptional();
				
				List<Optional> lop = new LinkedList<Optional>();
				List<OptionalTO> lopto = new LinkedList<OptionalTO>();
				if(nomeopt!=null){
				for(int i=0; nomeopt.size()>i; i++) {
					Optional op = new Optional(dop.getOptional(nomeopt.get(i)));
					lop.add(op);
					lopto.add(op);
				}
				}
				
				
				Prenotazione pre = new Prenotazione(new Partecipante(daopar.getPartecipante(usernameP)),
						new Gita(dg.getGita(IdGita)), datapren, lop);
			
				if(!pre.isPrenotazioneInStorage()) {
					daop.setPrenotazione(pre);
					if(nomeopt!=null)
						daop.setOptionalPrenotazione(pre, lopto);
				}
				else
					throw new OperazioneNonCompletata();
		}
		catch(DaoException e)
		{
			MessageDisplayer.showErrorMessage(e.toString());
		}

		
		
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.IdGita = (int) parameters.get(0);
		this.usernameP = (String) parameters.get(1);
		this.datapren = (LocalDate) parameters.get(2);
		this.nomeopt = (List<Integer>) parameters.get(3);
	}

}
