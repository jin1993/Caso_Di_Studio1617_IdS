package business.to;

public class ManagerTO {

	private String username;
	private String nome;
	private String cognome;
	private String email;
	
	public ManagerTO() {}
	
	public ManagerTO(String username, String nome, String cognome, String email) {
		init(username, nome, cognome, email);
	}
	
	public ManagerTO(ManagerTO manager) {
		init(manager.username, manager.nome, manager.cognome, manager.email);
	}
	
	private void init(String username, String nome, String cognome, String email) {
		this.cognome=cognome;
		this.username=username;
		this.nome=nome;
		this.email=email;
	}
	
	public ManagerTO getData() {
		return new ManagerTO(this);
	}
	
	public void setData(String username, String nome, String cognome, String email) {
		init(username, nome, cognome, email);
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
}
