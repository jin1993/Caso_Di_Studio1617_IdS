package business.to;

import java.time.LocalDate;
import java.util.List;

import business.bo.Gita;
import business.bo.Optional;
import business.bo.Partecipante;

public class PrenotazioneTO {
	
	private Partecipante partecipante;
	protected Gita gita;
	private LocalDate dataprenotazione;
	
	protected List<Optional> optional;
	
	public PrenotazioneTO() {
		// TODO Auto-generated constructor stub
	}
	
	public PrenotazioneTO(Partecipante partecipante, Gita gita, LocalDate dataprenotazione, List<Optional> optional) {
		init(partecipante, gita, dataprenotazione, optional);
	}

	public PrenotazioneTO(PrenotazioneTO prenotazione) {
		init(prenotazione.partecipante, prenotazione.gita, prenotazione.dataprenotazione, prenotazione.optional);
	}
	
	private void init(Partecipante partecipante, Gita gita, LocalDate dataprenotazione, List<Optional> optional) {
		this.dataprenotazione=dataprenotazione;
		this.gita=gita;
		this.partecipante=partecipante;
		this.optional=optional;
	}
	
	public PrenotazioneTO getData() {
		return new PrenotazioneTO(this);
	}
	
	public void setData(Partecipante partecipante, Gita gita, LocalDate dataprenotazione, List<Optional> optional) {
		init(partecipante, gita, dataprenotazione, optional);
	}
	
	public LocalDate getDataprenotazione() {
		return dataprenotazione;
	}
	
	public GitaTO getGita() {
		return gita;
	}
	
	public PartecipanteTO getPartecipante() {
		return partecipante;
	}
	
	public void setDataprenotazione(LocalDate dataprenotazione) {
		this.dataprenotazione = dataprenotazione;
	}
	
	public void setGita(Gita gita) {
		this.gita = gita;
	}
	
	public void setPartecipante(Partecipante partecipante) {
		this.partecipante = partecipante;
	}
	
	public List<Optional> getOptional() {
		return optional;
	}
	
	public void setOptional(List<Optional> optional) {
		this.optional = optional;
	}
}
