package business.to;

import java.time.LocalDate;

public class PartecipanteTO {
	
	//private int id;
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private String codf;
	//private enum Sesso {MALE, FEMALE};
	private Sesso sesso;
	private String via;
	private String ncivico;
	private String citta;
	private LocalDate datanascita;
	private int numtessanit;
	private String Certificato;
	private LocalDate datacert;
	
	public PartecipanteTO() {}
	
	public PartecipanteTO(String username, String email, String nome, String cognome, 
			String codf, Sesso sesso, String via, String citta, String ncivico,
			LocalDate datanascita, LocalDate datacert, int numtessanit) {
		
		init(username, email, nome, cognome, codf, sesso, via, ncivico, citta, datanascita, datacert, numtessanit);
	}
	
	public PartecipanteTO(PartecipanteTO partecipante) {
		init(partecipante.username, partecipante.email, partecipante.nome, partecipante.cognome, partecipante.codf, partecipante.sesso, partecipante.via, partecipante.ncivico, partecipante.citta, partecipante.datanascita, partecipante.datacert, partecipante.numtessanit);
	}
	
	private void init(String username, String email, String nome, String cognome, 
			String codf, Sesso sesso, String via, String ncivico, String citta,
			LocalDate datanascita, LocalDate datacert, int numtessanit) {
		this.username = username;
		this.email = email;
		this.nome = nome;
		this.cognome = cognome;
		this.codf=codf;
		this.citta=citta;
		this.datacert=datacert;
		this.datanascita=datanascita;
		this.ncivico=ncivico;
		this.numtessanit=numtessanit;
		this.sesso=sesso;
		this.via=via;
	}
	
	public PartecipanteTO getData() {
		return new PartecipanteTO(this);
	}
	
	public void setData(String username, String email, String nome, String cognome, 
			String codf, Sesso sesso, String via, String citta, String ncivico,
			LocalDate datanascita, LocalDate datacert, int numtessanit) {
		init(username, email, nome, cognome, codf, sesso, via, ncivico, citta, datanascita, datacert, numtessanit);
	}
	
	public String getCitta() {
		return citta;
	}
	
	public String getCodf() {
		return codf;
	}
	public String getCognome() {
		return cognome;
	}
	public LocalDate getDatacert() {
		return datacert;
	}
	
	public LocalDate getDatanascita() {
		return datanascita;
	}
	public String getEmail() {
		return email;
	}
	
	public String getNcivico() {
		return ncivico;
	}
	
	public String getNome() {
		return nome;
	}
	
	public int getNumtessanit() {
		return numtessanit;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getVia() {
		return via;
	}
	
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	public void setCodf(String codf) {
		this.codf = codf;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public void setDatacert(LocalDate datacert) {
		this.datacert = datacert;
	}
	
	public void setDatanascita(LocalDate datanascita) {
		this.datanascita = datanascita;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setNcivico(String ncivico) {
		this.ncivico = ncivico;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setNumtessanit(int numtessanit) {
		this.numtessanit = numtessanit;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setVia(String via) {
		this.via = via;
	}
	
	public Sesso getSesso() {
		return sesso;
	}
	
	public void setSesso(Sesso sesso) {
		this.sesso = sesso;
	}
	
	public String getCertificato() {
		return Certificato;
	}
	
	public void setCertificato(String certificato) {
		Certificato = certificato;
	}
}
