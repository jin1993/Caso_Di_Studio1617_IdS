package business.to;

public class OptionalTO {
	
	private int id;
	private String nome;
	private float costo;
	
	public OptionalTO() {}
	
	public OptionalTO(int id, String nome, float costo) {
		init(id, nome, costo);
	}
	
	public OptionalTO(OptionalTO optional) {
		init(optional.id, optional.nome, optional.costo);
	}
	
	private void init(int id, String nome, float costo) {
		this.costo=costo;
		this.id=id;
		this.nome=nome;
	}
	
	public OptionalTO getData() {
		return new OptionalTO(this);
	}
	
	public void setData(int id, String nome, float costo) {
		init(id, nome, costo);
	}
	
	public float getCosto() {
		return costo;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setCosto(float costo) {
		this.costo = costo;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
