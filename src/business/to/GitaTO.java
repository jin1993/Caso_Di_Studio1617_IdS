package business.to;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import business.bo.Manager;
import business.bo.Optional;

public class GitaTO {
	
	private int id;
	private LocalDate data;
	//private enum Tipo {MONTAGNA, MONGOLFIERA};
	private Tipo tipo;
	private int minpart;
	private int maxpart;
	private float costo;
	//private enum Stato {INCORSO, TERMINATA, ANNULLATA, SOLDOUT, DASVOLGERE};
	private Stato stato;
	private Manager manager;
	
	protected List<Optional> optional;
	
	public GitaTO() {}
	
	public GitaTO(int id, LocalDate data, Tipo tipo, int min, int max, float costo, 
			Stato stato, Manager manager, List<Optional> optional) {
		init(id, data, tipo, min, max, costo, stato, manager, optional);
	}

	public GitaTO(GitaTO gita) {
		init(gita.id, gita.data, gita.tipo, gita.minpart, gita.maxpart, gita.costo, gita.stato, gita.manager, gita.optional);
	}
	
	private void init(int id, LocalDate data, Tipo tipo, int min, int max, float costo, 
			Stato stato, Manager manager, List<Optional> optional) {
		this.id=id;
		this.data=data;
		this.tipo=tipo;
		this.minpart=min;
		this.maxpart=max;
		this.costo=costo;
		this.stato=stato;
		this.manager=manager;
		this.optional=optional;
	}
	
	public GitaTO getDate() {
		return new GitaTO(this);
	}
	
	public void setData(int id, LocalDate data, Tipo tipo, int min, int max, float costo, 
			Stato stato, Manager manager, List<Optional> optional) {
		init(id, data, tipo, min, max, costo, stato, manager, optional);
	}
	
	public void setData(LocalDate data) {
		this.data=data;
	}
	
	public LocalDate getData() {
		return data;
	}
	
	public List<Optional> getOptionalGita(){
		List<Optional> accList = new LinkedList<Optional>();
		for(Optional o : optional) {
			accList.add(new Optional(o.getId(), o.getNome(), o.getCosto()));
		}
		return accList;
	}
	
	public float getCosto() {
		return costo;
	}
	
	public int getId() {
		return id;
	}
	
	public Manager getManager() {
		return manager;
	}
	
	public int getMaxpart() {
		return maxpart;
	}
	
	public int getMinpart() {
		return minpart;
	}
	
	public List<Optional> getOptional() {
		return optional;
	}
	
	public Stato getStato() {
		return stato;
	}
	
	public Tipo getTipo() {
		return tipo;
	}
	
	public void setCosto(float costo) {
		this.costo = costo;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setManager(Manager manager) {
		this.manager = manager;
	}
	
	public void setMaxpart(int maxpart) {
		this.maxpart = maxpart;
	}
	
	public void setMinpart(int minpart) {
		this.minpart = minpart;
	}
	
	public void setOptional(List<Optional> optional) {
		this.optional = optional;
	}
	
	public void setStato(Stato stato) {
		this.stato = stato;
	}
	
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
		
}

