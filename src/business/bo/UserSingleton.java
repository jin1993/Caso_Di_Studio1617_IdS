package business.bo;


public class UserSingleton {

	private static UserSingleton singleton = null;
	private Partecipante userp = null;
	private Manager userm = null;
	private Gita gcorrente = null;
	
	private UserSingleton() {
	}

	public Gita getGcorrente() {
		return gcorrente;
	}
	
	public void setGcorrente(Gita gcorrente) {
		this.gcorrente = gcorrente;
	}
	public void setPartecipante(Partecipante p) {
		this.userp = p;
	}

	
	public Partecipante getPartecipante() {
		return userp;
	}

	public void setManager(Manager p) {
		this.userm = p;
	}

	
	public Manager getManager() {
		return userm;
	}
	
	public static UserSingleton getInstance() {
		if (singleton == null) {
			singleton = new UserSingleton();
		}
		return singleton;
	}
}
