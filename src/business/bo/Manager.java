package business.bo;

import business.OperazioneNonCompletata;
import business.to.ManagerTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoManager;

public class Manager extends ManagerTO {

	public Manager() {
		super();
	}
	
	public Manager(String username, String nome, String cognome, String email) {
		super(username, nome, cognome, email);
	}
	
	public Manager(ManagerTO man) {
		super(man);
	}
	
	public boolean isManagerInStorageByUsername() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoManager daoManager = daoFactory.getDaoManager();
			
			daoManager.getManager(this.getUsername());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}

}
