package business.bo;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.GitaTO;
import business.to.ManagerTO;
import business.to.OptionalTO;
import business.to.Stato;
import business.to.Tipo;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoGita;

public class Gita extends GitaTO{

	public Gita(int id, LocalDate data, Tipo tipo, int min, int max, float costo, 
			Stato stato, Manager manager, List<Optional> optional) {
		super(id, data, tipo, min, max, costo, stato, manager, optional);
	}
	
	public Gita() {
		super();
	}
	
	public Gita(GitaTO gita) {
		super(gita);
	}
	
	public void aggiungiOptional(Optional op) throws OperazioneNonCompletata{
		List<Optional> listO = getOptionalGita();
		
		OptionalTO opt = new OptionalTO(op);
		
		if(!listO.contains(opt)) {
			optional.add(op);
		}
		else {
			throw new OperazioneNonCompletata("Optional gi� presente nella gita");
		}
	
	}
	
	public void deleteOptional(Optional op) {
		boolean isPresent=false;
		
		for(Iterator<Optional> iterOp = optional.iterator(); iterOp.hasNext() && isPresent==false;) {
			
			OptionalTO opt = iterOp.next();
			
			if(opt.getId()==(op.getId())) {
				isPresent = true;
				iterOp.remove();
			}
		}
		
	}
	
	public boolean isGitaInStorageById() throws OperazioneNonCompletata{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoGita daoGita = daoFactory.getDaoGita();
			
			daoGita.getGita(this.getId());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
	
	public boolean isGitaInStorageByData() throws OperazioneNonCompletata{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoGita daoGita = daoFactory.getDaoGita();
			
			daoGita.getGita(this.getData());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
}
