package business.bo;

import java.time.LocalDate;

import business.OperazioneNonCompletata;
import business.to.PartecipanteTO;
import business.to.Sesso;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoPartecipante;

public class Partecipante extends PartecipanteTO {
	
	public Partecipante() {
		super();
	}
	
	public Partecipante(String username, String email, String nome, String cognome, 
			String codf, Sesso sesso, String via, String citta, String ncivico,
			LocalDate datanascita, LocalDate datacert, int numtessanit) {
		
		super(username, email, nome, cognome, codf, sesso, via, ncivico, citta, datanascita, datacert, numtessanit);
	}

	public Partecipante(PartecipanteTO par) {
		super(par);
	}
	
	public boolean isPartecipanteInStorageByUsername() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoPartecipante daoPart= daoFactory.getDaoPartecipante();
			
			daoPart.getPartecipante(this.getUsername());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
	
	public boolean isPartecipanteInStorageByEmail() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoPartecipante daoPart= daoFactory.getDaoPartecipante();
			
			daoPart.getPartecipante(this.getEmail());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
	
}
