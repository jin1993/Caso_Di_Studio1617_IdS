package business.bo;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.GitaTO;
import business.to.OptionalTO;
import business.to.PartecipanteTO;
import business.to.PrenotazioneTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoPrenotazione;

public class Prenotazione extends PrenotazioneTO {

	public Prenotazione() {
		super();
	}
	
	public Prenotazione(Partecipante partecipante, Gita gita, LocalDate dataprenotazione, List<Optional> optional) {
		super(partecipante, gita, dataprenotazione, optional);
	}

	public Prenotazione(PrenotazioneTO pre) {
		super(pre);
	}
	
	public void aggiungiOptional(Optional op) throws OperazioneNonCompletata{
		List<Optional> listO = gita.getOptionalGita();
		
		Optional opt = new Optional(op);
		
		if(!listO.contains(opt)) {
			optional.add(op);
		}
		else {
			throw new OperazioneNonCompletata("Optional gi� presente nella gita");
		}
	
	}
	
	public void deleteOptional(Optional op) {
		boolean isPresent=false;
		
		for(Iterator<Optional> iterOp = gita.getOptionalGita().iterator(); iterOp.hasNext() && isPresent==false;) {
			
			OptionalTO opt = iterOp.next();
			
			if(opt.getId()==(op.getId())) {
				isPresent = true;
				iterOp.remove();
			}
		}
		
	}
	
	/*
	 * 
	 */
	
	public boolean isPrenotazioneInStorage() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoPrenotazione daoPre = daoFactory.getDaoPrenotazione();
			List<PrenotazioneTO> listPre = new LinkedList<PrenotazioneTO>();
			
			listPre = daoPre.getPrenotazioni();
			
			Iterator<PrenotazioneTO> preIt = listPre.iterator();
			
			while(preIt.hasNext() && isPresent==false) {
				PrenotazioneTO p = preIt.next();
				
				if(p.getData().equals(this))
				{
				
					isPresent = true;
				}
			}
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
}


