package business.bo;

import business.OperazioneNonCompletata;
import business.to.OptionalTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoOptional;

public class Optional extends OptionalTO{
	public Optional() {
		super();
	}
	
	public Optional(int id, String nome, float costo) {
		super(id, nome, costo);
	}
	
	public Optional(OptionalTO op) {
		super(op);
	}
	
	public boolean isOptionalInStorage() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoOptional dao = daoFactory.getDaoOptional();
			
			dao.getOptional(this.getNome());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
}
